<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\Type\TemplateExtendsTypeBlock;

final readonly class RepeatedTemplateTypeName implements ConstraintViolation
{
    public function __construct(
        public string $name,
        public TemplateExtendsTypeBlock $cause
    ) {}

    public function getMessage(): string
    {
        return sprintf('Repeated template name of %s', $this->name);
    }
}
