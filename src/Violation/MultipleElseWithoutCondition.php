<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class MultipleElseWithoutCondition implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Multiple else cases without condition';
    }
}
