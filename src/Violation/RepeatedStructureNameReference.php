<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\Structure\Class\ImplementedInterfacesBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Class\InterfaceReferenceBlock;

final class RepeatedStructureNameReference implements ConstraintViolation
{
    public function __construct(
        public string $name,
        public ImplementedInterfacesBlock $block,
        public InterfaceReferenceBlock $cause
    ) {}

    public function getMessage(): string
    {
        return sprintf('Repeated interface reference of %s', $this->name);
    }
}
