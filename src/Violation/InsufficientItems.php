<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AtLeastOneItem;

final class InsufficientItems implements ConstraintViolation
{
    public function __construct(
        public CollectiveBlockInterface $block,
        public AtLeastOneItem $constraint
    ) {}

    public function getMessage(): string
    {
        return 'At least one item must be provided';
    }
}
