<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class RepeatedElseWithoutCondition implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Repeated else statement without condition';
    }
}
