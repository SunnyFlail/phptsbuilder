<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\ExportBlockInterface;

final class RepeatedExportName implements ConstraintViolation
{
    public function __construct(
        private string $name,
        private ExportBlockInterface $cause
    ) {}

    public function getMessage(): string
    {
        return sprintf('Repeated export item named %s', $this->name);
    }
}
