<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionParameterBlockInterface;

final readonly class RepeatedParameterName implements ConstraintViolation
{
    public function __construct(
        public string $parameterName,
        public FunctionParameterBlockInterface $parameter,
    ) {}

    public function getMessage(): string
    {
        return sprintf(
            'Repeated parameter named %s',
            $this->parameterName,
        );
    }
}
