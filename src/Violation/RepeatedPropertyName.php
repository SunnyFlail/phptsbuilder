<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\StructurePropertyBlockInterface;

final readonly class RepeatedPropertyName implements ConstraintViolation
{
    public function __construct(
        public string $propertyName,
        public StructurePropertyBlockInterface $property,
    ) {}

    public function getMessage(): string
    {
        return sprintf('Repeated property named %s', $this->propertyName);
    }
}
