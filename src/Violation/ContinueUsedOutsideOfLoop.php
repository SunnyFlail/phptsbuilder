<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class ContinueUsedOutsideOfLoop implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Cannot use continue statement outside of a loop';
    }
}
