<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlockInterface;

final class NoReturnStatementInBody implements ConstraintViolation
{
    public function __construct(private FunctionBlockInterface $block) {}

    public function getMessage(): string
    {
        return 'No return statements found in function body';
    }
}
