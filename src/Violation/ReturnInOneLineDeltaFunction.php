<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class ReturnInOneLineDeltaFunction implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Return block in one line delta function';
    }
}
