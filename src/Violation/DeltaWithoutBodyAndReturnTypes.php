<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class DeltaWithoutBodyAndReturnTypes implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Delta function must have return type or body';
    }
}
