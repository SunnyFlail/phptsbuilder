<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class EndingStatementIsNotLastBodyItem implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Ending statement must be last item in a body';
    }
}
