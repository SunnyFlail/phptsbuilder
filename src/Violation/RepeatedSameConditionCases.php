<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\Control\Switch\CaseBlockInterface;

final readonly class RepeatedSameConditionCases implements ConstraintViolation
{
    public function __construct(public string $condition, public CaseBlockInterface $cause) {}

    public function getMessage(): string
    {
        return sprintf(
            'Used same condition (%s) multiple times in one switch',
            $this->condition
        );
    }
}
