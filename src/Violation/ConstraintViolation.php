<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

interface ConstraintViolation
{
    public function getMessage(): string;
}
