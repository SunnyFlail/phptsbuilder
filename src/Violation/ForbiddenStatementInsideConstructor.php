<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;

final readonly class ForbiddenStatementInsideConstructor implements ConstraintViolation
{
    public function __construct(public BlockInterface $cause) {}

    public function getMessage(): string
    {
        return sprintf(
            'Used forbidden statement %s inside constructor',
            str_replace('Block', '', $this->cause::class)
        );
    }
}
