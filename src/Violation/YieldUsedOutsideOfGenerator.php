<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class YieldUsedOutsideOfGenerator implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Yield used outside of a generator function';
    }
}
