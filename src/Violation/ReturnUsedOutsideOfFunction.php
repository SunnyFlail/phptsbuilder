<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class ReturnUsedOutsideOfFunction implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Return statement used outside a function';
    }
}
