<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class BreakUsedOutsideOfLoopOrSwitch implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Cannot use break outside of a loop or switch';
    }
}
