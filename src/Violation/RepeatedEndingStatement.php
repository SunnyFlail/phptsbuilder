<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\BodyBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\EndingStatementBlockInterface;

final readonly class RepeatedEndingStatement implements ConstraintViolation
{
    public function __construct(
        public int $index,
        public BodyBlockInterface $body,
        public EndingStatementBlockInterface $cause,
    ) {}

    public function getMessage(): string
    {
        return sprintf(
            'Repeated directly placed ending statement. Caused by item number %d of type %s',
            $this->index,
            $this->cause::class
        );
    }
}
