<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

final class AwaitUsedOutsideAsyncFunction implements ConstraintViolation
{
    public function getMessage(): string
    {
        return 'Cannot use await outside async functions';
    }
}
