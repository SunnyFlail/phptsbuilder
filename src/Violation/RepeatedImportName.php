<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Violation;

use SunnyFlail\PhpTsBuilder\Block\ImportBlockInterface;

final class RepeatedImportName implements ConstraintViolation
{
    public function __construct(
        private string $name,
        private ImportBlockInterface $cause
    ) {}

    public function getMessage(): string
    {
        return sprintf('Repeated import item named %s', $this->name);
    }
}
