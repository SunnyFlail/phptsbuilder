<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Exception;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Validator\BlockDeclaration;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

final class BlockValidationFailureException extends \Exception
{
    /**
     * @var ConstraintViolation[]
     */
    public readonly array $violations;

    public function __construct(
        public readonly BlockInterface $block,
        public readonly BlockDeclaration $blockDeclaration,
        ConstraintViolation ...$violations
    ) {
        $this->violations = $violations;
    }

    public function __toString(): string
    {
        return sprintf(
            'Constraints failed for block %2$s%1$s%1$sViolations:%1$s%3$s%1$s%4$s%1$s%1$sCode:%1$s%5$s',
            PHP_EOL,
            $this->block::class,
            $this->buildViolationsString(),
            $this->blockDeclaration->getTraceAsString(),
            implode('', $this->blockDeclaration->codeLines)
        );
    }

    private function buildViolationsString(): string
    {
        return implode(
            PHP_EOL,
            array_map(
                fn (ConstraintViolation $violation) => sprintf(
                    '%s: %s',
                    $violation::class,
                    $violation->getMessage()
                ),
                $this->violations
            )
        );
    }
}
