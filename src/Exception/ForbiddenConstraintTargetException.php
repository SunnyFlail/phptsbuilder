<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Exception;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

final class ForbiddenConstraintTargetException extends \InvalidArgumentException
{
    public function __construct(
        public readonly Constraint $constraint,
        public readonly BlockInterface $block
    ) {
        parent::__construct(
            sprintf(
                'Cannot use %s constraint on instances of %s',
                $constraint::class,
                $block::class
            )
        );
    }
}
