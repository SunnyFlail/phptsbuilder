<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\BlockItemsConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

/**
 * @template-extends AbstractItemConstraintRunner<CollectiveBlockInterface,BlockItemsConstraint>
 */
final class BlockItemsConstraintRunner extends AbstractItemConstraintRunner
{
    protected function canRun(Constraint $constraint): bool
    {
        return $constraint instanceof BlockItemsConstraint;
    }

    /**
     * @param CollectiveBlockInterface $block
     *
     * @return iterable<int,BlockInterface>
     */
    protected function traverse(BlockInterface $block): iterable
    {
        foreach ($block->getItems() as $index => $item) {
            yield $index => $item;
        }
    }
}
