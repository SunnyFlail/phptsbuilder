<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

interface ConstraintRunnerInterface
{
    /**
     * @return iterable<ConstraintViolation>
     */
    public function run(
        BlockInterface $block,
        Constraint ...$constraints
    ): iterable;
}
