<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\BlockConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;

final class BlockConstraintRunner implements ConstraintRunnerInterface
{
    use IsATrait;

    /**
     * @param BlockConstraint $constraints
     */
    public function run(BlockInterface $block, Constraint ...$constraints): iterable
    {
        foreach ($constraints as $constraint) {
            if ($this->isA($constraint, BlockConstraint::class)) {
                yield from $constraint->validate($block);
            }
        }
    }
}
