<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;

interface BlockDeclarationTracerInterface
{
    public function traceBlockDeclaration(BlockInterface $block): BlockDeclaration;
}
