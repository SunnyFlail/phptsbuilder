<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;

final class ValidatorFacade
{
    private static ?ValidatorInterface $validator = null;
    private static bool $disabled = false;

    private function __construct() {}

    public static function disable(): void
    {
        self::$disabled = true;
    }

    public static function setValidator(ValidatorInterface $validator): void
    {
        self::$validator = $validator;
    }

    public static function validate(BlockInterface $block): void
    {
        if (!self::$validator) {
            self::$validator = new Validator(
                new AttributeConstraintProvider(),
                new BlockDeclarationTracer(),
                new ConstraintRunnerAggregate(
                    new BlockConstraintRunner(),
                    new BlockItemsConstraintRunner(),
                    new BlockBodiesConstraintRunner(),
                )
            );
        }

        if (!self::$disabled) {
            self::$validator->validateBlock($block);
        }
    }
}
