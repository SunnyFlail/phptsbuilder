<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

final readonly class BlockDeclaration
{
    /**
     * @param array{
     *      lineBefore: string|null,
     *      line: string,
     *      lineAfter: string|null
     * } $codeLines
     * @param array<array{
     *      class: class-string|null,
     *      file: string|null,
     *      function: string|null,
     *      line: int|null,
     *      type: '->'|'::'|null
     * }> $trace
     */
    public function __construct(
        public string $fileName,
        public int $line,
        public array $codeLines,
        public array $trace,
    ) {}

    public function getTraceAsString(): string
    {
        $lines = ['Block initialization trace:'];

        foreach ($this->trace as $index => $trace) {
            $lines[] = sprintf(
                '#%s %s(%s): %s%s%s',
                $index,
                $trace['file'] ?? '',
                $trace['line'] ?? '',
                $trace['class'] ?? '',
                $trace['type'] ?? '',
                !isset($trace['function']) ? '' : sprintf('%s()', $trace['function']),
            );
        }

        return implode(PHP_EOL, $lines);
    }
}
