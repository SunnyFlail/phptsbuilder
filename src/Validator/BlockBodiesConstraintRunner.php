<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\BlockBodiesConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

/**
 * @template-extends AbstractItemConstraintRunner<BodyContainingBlockInterface,BlockBodiesConstraint>
 */
final class BlockBodiesConstraintRunner extends AbstractItemConstraintRunner
{
    protected function canRun(Constraint $constraint): bool
    {
        return $constraint instanceof BlockBodiesConstraint;
    }

    /**
     * @return iterable<int,BlockInterface>
     */
    protected function traverse(BlockInterface $block): iterable
    {
        /** @var BodyContainingBlockInterface $block */
        foreach ($block->getBodies() as $body) {
            foreach ($body->getItems() as $item) {
                if (
                    $item instanceof BodyContainingBlockInterface
                    && !($item instanceof FunctionBlockInterface)
                ) {
                    yield from $this->traverse($item);
                } else {
                    yield $item;
                }
            }
        }
    }
}
