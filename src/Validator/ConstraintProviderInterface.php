<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

interface ConstraintProviderInterface
{
    /**
     * @return iterable<int,Constraint>
     */
    public function getBlockConstraints(BlockInterface $block): iterable;
}
