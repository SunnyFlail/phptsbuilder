<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

final class AttributeConstraintProvider implements ConstraintProviderInterface
{
    /**
     * @return iterable<int,Constraint>
     */
    public function getBlockConstraints(BlockInterface $block): iterable
    {
        $reflection = new \ReflectionObject($block);

        foreach ($reflection->getAttributes(Constraint::class) as $attribute) {
            yield $attribute->newInstance();
        }
    }
}
