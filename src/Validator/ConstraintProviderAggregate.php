<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

final class ConstraintProviderAggregate implements ConstraintProviderInterface
{
    /**
     * @var ConstraintProviderInterface[]
     */
    private array $constraintProviders;

    public function __construct(ConstraintProviderInterface ...$constraintProviders)
    {
        $this->constraintProviders = $constraintProviders;
    }

    /**
     * @return iterable<int,Constraint>
     */
    public function getBlockConstraints(BlockInterface $block): iterable
    {
        foreach ($this->constraintProviders as $provider) {
            yield from $provider->getBlockConstraints($block);
        }
    }
}
