<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\BlockValidationFailureException;

final class Validator implements ValidatorInterface
{
    public function __construct(
        private ConstraintProviderInterface $constraintProvider,
        private BlockDeclarationTracerInterface $blockDeclarationTracer,
        private ConstraintRunnerInterface $constraintRunner
    ) {}

    public function validateBlock(BlockInterface $block): void
    {
        $violations = [];

        foreach (
            $this->constraintRunner->run(
                $block,
                ...$this->constraintProvider->getBlockConstraints($block)
            ) as $violation
        ) {
            $violations[] = $violation;
        }

        if ($violations) {
            throw new BlockValidationFailureException($block, $this->blockDeclarationTracer->traceBlockDeclaration($block), ...$violations);
        }
    }
}
