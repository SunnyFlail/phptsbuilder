<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;

final class BlockDeclarationTracer implements BlockDeclarationTracerInterface
{
    public function traceBlockDeclaration(BlockInterface $block): BlockDeclaration
    {
        try {
            throw new \Exception();
        } catch (\Throwable $e) {
            return $this->getBlockDeclaration($block, $e);
        }
    }

    private function getBlockDeclaration(BlockInterface $block, \Throwable $e): BlockDeclaration
    {
        $traces = array_reverse($e->getTrace());
        $tracesBefore = [];

        foreach ($traces as $trace) {
            $tracesBefore[] = $trace;

            if ($this->isBlockDeclarationTrace($block, $trace)) {
                return new BlockDeclaration(
                    $trace['file'] ?? '',
                    $trace['line'] ?? 0,
                    $this->getCode($trace),
                    array_reverse($tracesBefore),
                );
            }
        }

        throw new \Exception();
    }

    private function isBlockDeclarationTrace(BlockInterface $block, array $trace): bool
    {
        return
            isset($trace['function'])
            && isset($trace['class'])
            && '__construct' === $trace['function']
            && is_a($block, $trace['class'])
        ;
    }

    /**
     * @param array{file:string,line:int}
     */
    private function getCode(array $trace): array
    {
        $file = new \SplFileObject($trace['file']);

        $lines = [];
        $lineIndex = $trace['line'] - 1;

        $lines['lineBefore'] = $this->getLine($file, $lineIndex - 1);
        $lines['line'] = $this->getLine($file, $lineIndex);
        $lines['lineAfter'] = $this->getLine($file, $lineIndex + 1);

        return $lines;
    }

    private function getLine(\SplFileObject $file, int $line): ?string
    {
        $file->seek($line);

        if ($file->eof()) {
            return null;
        }

        return $file->getCurrentLine();
    }
}
