<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;

final class ConstraintRunnerAggregate implements ConstraintRunnerInterface
{
    /**
     * @var ConstraintRunnerInterface[]
     */
    private array $runners;

    public function __construct(ConstraintRunnerInterface ...$runners)
    {
        $this->runners = $runners;
    }

    public function run(BlockInterface $block, Constraint ...$constraints): iterable
    {
        foreach ($this->runners as $runner) {
            yield from $runner->run($block, ...$constraints);
        }
    }
}
