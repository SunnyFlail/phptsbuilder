<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;

interface ValidatorInterface
{
    public function validateBlock(BlockInterface $block): void;
}
