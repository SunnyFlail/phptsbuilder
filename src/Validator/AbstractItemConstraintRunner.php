<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Validator;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;
use SunnyFlail\PhpTsBuilder\Constraint\ItemConstraint;

/**
 * @template-covariant TBlock of BlockInterface
 * @template-covariant TConstraint of ItemConstraint
 */
abstract class AbstractItemConstraintRunner implements ConstraintRunnerInterface
{
    /**
     * @param ItemConstraint $constraints
     */
    public function run(BlockInterface $block, Constraint ...$constraints): iterable
    {
        /** @var TConstraint[] */
        $handledConstraints = [];

        foreach ($constraints as $constraint) {
            if ($this->canRun($constraint)) {
                $constraint->startValidation($block);
                $handledConstraints[] = $constraint;
            }
        }

        if (!$handledConstraints) {
            return [];
        }

        foreach ($this->traverse($block) as $index => $item) {
            foreach ($handledConstraints as $constraint) {
                $constraint->validateItem($block, $item, $index);
            }
        }

        foreach ($handledConstraints as $constraint) {
            yield from $constraint->getViolations($block);
        }
    }

    /**
     * @psalm-assert-if-true TConstraint $constraint
     */
    abstract protected function canRun(Constraint $constraint): bool;

    /**
     * @param TBlock $block
     *
     * @return iterable<BlockInterface>
     */
    abstract protected function traverse(BlockInterface $block): iterable;
}
