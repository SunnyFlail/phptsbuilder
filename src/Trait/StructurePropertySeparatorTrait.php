<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait StructurePropertySeparatorTrait
{
    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return sprintf('%1$s%1$s', $settings->newLineChar);
    }
}
