<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait BuildControlBlockDeclarationTrait
{
    use IsMultilineTrait;
    use SpacingTrait;

    protected function buildControlBlockDeclaration(
        TypescriptCodeSettings $settings,
        string $beforeBracket,
        BlockInterface $bracketContents,
        string $afterBracket
    ): string {
        $declaration = sprintf(
            '%s (%s)%s',
            $beforeBracket,
            '%1$s%2$s%1$s',
            $afterBracket
        );
        $condition = $this->addSpacing($bracketContents, $settings);
        $isMultiline = $this->isMultiline(
            $settings,
            $declaration,
            '%1$s%2$s%1$s',
            $condition
        );

        return sprintf(
            $declaration,
            $isMultiline ? $settings->newLineChar : '',
            $isMultiline ? $condition : trim($condition)
        );
    }
}
