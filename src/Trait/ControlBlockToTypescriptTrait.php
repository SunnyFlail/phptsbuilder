<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait ControlBlockToTypescriptTrait
{
    use SpacingTrait;

    public readonly ControlBodyBlock $body;

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return implode(
            $settings->newLineChar,
            [
                $this->buildStartBlock($settings),
                $this->addSpacing($this->body, $settings),
                $this->buildEndBlock($settings),
            ]
        );
    }

    abstract protected function buildStartBlock(TypescriptCodeSettings $settings): string;

    abstract protected function buildEndBlock(TypescriptCodeSettings $settings): string;
}
