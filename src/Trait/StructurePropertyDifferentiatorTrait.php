<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait StructurePropertyDifferentiatorTrait
{
    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s%s',
            $settings->structurePropertyDifferentiator->value,
            $settings->newLineChar
        );
    }
}
