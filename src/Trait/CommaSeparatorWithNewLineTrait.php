<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait CommaSeparatorWithNewLineTrait
{
    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return sprintf(',%s', $settings->newLineChar);
    }
}
