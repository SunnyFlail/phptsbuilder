<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlockInterface;

trait TraverseBodiesTrait
{
    /**
     * @return iterable<BlockInterface>
     */
    protected function traverseBodies(BodyContainingBlockInterface $block): iterable
    {
        foreach ($block->getBodies() as $body) {
            foreach ($body->getItems() as $item) {
                if (
                    $item instanceof BodyContainingBlockInterface
                    && !($item instanceof FunctionBlockInterface)
                ) {
                    yield from $this->traverseBodies($item);
                } else {
                    yield $item;
                }
            }
        }
    }
}
