<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait PassthroughItemCallbackTrait
{
    protected function itemCallback(BlockInterface $item, TypescriptCodeSettings $settings): string
    {
        return $item->toTypescript($settings);
    }
}
