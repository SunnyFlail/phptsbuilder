<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait SpacingTrait
{
    protected function addSpacing(string|BlockInterface $line, TypescriptCodeSettings $settings): string
    {
        if ($line instanceof BlockInterface) {
            $line = sprintf(
                '%s%s',
                $settings->lineStartWhitespace,
                $line->toTypescript($settings)
            );
        }

        return preg_replace(
            sprintf('/(%s)/', $this->escapeSpecialCharacters($settings->newLineChar)),
            sprintf('$1%s', $settings->lineStartWhitespace),
            $line
        );
    }

    private function escapeSpecialCharacters(string $string): string
    {
        return str_ireplace(
            ['\\', '$'],
            ['\\\\', '\$'],
            $string
        );
    }
}
