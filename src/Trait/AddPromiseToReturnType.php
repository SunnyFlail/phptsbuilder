<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\Type\PromiseVariableTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;

trait AddPromiseToReturnType
{
    protected function addPromiseToReturnType(
        bool $async,
        VariableTypeBlockInterface $returnType
    ): VariableTypeBlockInterface {
        if (!$async || $returnType instanceof PromiseVariableTypeBlock) {
            return $returnType;
        }

        return new PromiseVariableTypeBlock($returnType);
    }
}
