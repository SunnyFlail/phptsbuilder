<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait IsMultilineTrait
{
    protected function isMultiline(
        TypescriptCodeSettings $settings,
        string $template,
        string $placeholders,
        string $replacement
    ): bool {
        return str_contains($replacement, $settings->newLineChar) || (
            mb_strlen($template) -
            mb_strlen($placeholders) +
            mb_strlen($replacement) >=
            $settings->lineLengthLimit
        );
    }
}
