<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait LineEndSemicolonItemCallbackTrait
{
    use IsEmptyLineTrait;

    protected function itemCallback(BlockInterface $item, TypescriptCodeSettings $settings): string
    {
        $line = $item->toTypescript($settings);

        if (
            $this->isEmptyLine($line, $settings)
            || $item instanceof MultilineBlockInterface
        ) {
            return $line;
        }

        return $line.';';
    }
}
