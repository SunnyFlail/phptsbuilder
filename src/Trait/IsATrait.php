<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

trait IsATrait
{
    /**
     * @template TStructure
     *
     * @param class-string<TStructure> $classNames
     *
     * @psalm-assert-if-true TStructure $object
     */
    protected function isA(object $object, string ...$classNames): bool
    {
        foreach ($classNames as $className) {
            if (is_a($object, $className)) {
                return true;
            }
        }

        return false;
    }
}
