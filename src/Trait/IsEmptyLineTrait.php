<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait IsEmptyLineTrait
{
    protected function isEmptyLine(?string $line, TypescriptCodeSettings $settings): bool
    {
        if (!$line) {
            return true;
        }

        $line = trim($line);

        return !$line
            || preg_match(
                sprintf(
                    '/^(?:%1$s)*\w*[,;\{\}\[\(]*\w*(?:%1$s)*$/',
                    $settings->newLineChar
                ),
                $line
            )
        ;
    }
}
