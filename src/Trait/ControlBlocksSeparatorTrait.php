<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Trait;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

trait ControlBlocksSeparatorTrait
{
    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return ' ';
    }
}
