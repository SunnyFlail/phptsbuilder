<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

/**
 * @template-extends UniqueCollectiveBlockInterface<StructurePropertyBlockInterface>
 */
interface StructurePropertiesContainerInterface extends UniqueCollectiveBlockInterface {}
