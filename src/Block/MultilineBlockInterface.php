<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

interface MultilineBlockInterface extends BlockInterface {}
