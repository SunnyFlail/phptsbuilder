<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

/**
 * @template-covariant TItem of BlockInterface
 */
interface CollectiveBlockInterface extends BlockInterface
{
    public function getItemCount(): int;

    /**
     * @return array<int,TItem>
     */
    public function getItems(): array;
}
