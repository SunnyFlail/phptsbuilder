<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

/**
 * @template-covariant TBody of BodyBlockInterface
 */
interface BodyContainingBlockInterface extends MultilineBlockInterface
{
    /**
     * @return iterable<TBody>
     */
    public function getBodies(): iterable;

    public function getBodiesCount(): int;
}
