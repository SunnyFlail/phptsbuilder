<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\Reusable\UniqueCollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueStructurePropertyNames;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends UniqueCollectiveBlock<ObjectPropertyInitializationBlock>
 */
#[UniqueStructurePropertyNames()]
final readonly class ObjectInitializationBlock extends UniqueCollectiveBlock implements ValueBlockInterface
{
    use CommaSeparatorWithNewLineTrait;
    use SpacingItemCallbackTrait;

    public function __construct(ObjectPropertyInitializationBlock ...$properties)
    {
        parent::__construct(...$properties);
    }

    final public function toTypescript(TypescriptCodeSettings $settings): string
    {
        if (0 === $this->getItemCount()) {
            return '{}';
        }

        return implode(
            $settings->newLineChar,
            [
                '{',
                parent::toTypescript($settings),
                '}',
            ]
        );
    }
}
