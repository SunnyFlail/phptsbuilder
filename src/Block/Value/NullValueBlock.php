<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class NullValueBlock implements ValueBlockInterface
{
    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return 'null';
    }
}
