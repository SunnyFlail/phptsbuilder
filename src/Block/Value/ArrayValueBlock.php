<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<ValueBlockInterface>
 */
final readonly class ArrayValueBlock extends CollectiveBlock implements ValueBlockInterface
{
    use CommaSeparatorWithNewLineTrait;
    use SpacingItemCallbackTrait;

    public function __construct(ValueBlockInterface ...$values)
    {
        parent::__construct(...$values);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return implode(
            $settings->newLineChar,
            [
                '[',
                parent::toTypescript($settings),
                ']',
            ]
        );
    }
}
