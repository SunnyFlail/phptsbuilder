<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class TernaryOperatorBlock implements ValueBlockInterface
{
    public function __construct(
        public ValueBlockInterface $condition,
        public ?ValueBlockInterface $truthValue,
        public ValueBlockInterface $falseValue
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s ?%s: %s',
            $this->condition->toTypescript($settings),
            !$this->truthValue ? '' : sprintf(
                ' %s ',
                $this->truthValue->toTypescript($settings)
            ),
            $this->falseValue->toTypescript($settings)
        );
    }
}
