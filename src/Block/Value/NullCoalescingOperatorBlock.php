<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class NullCoalescingOperatorBlock implements ValueBlockInterface
{
    public function __construct(
        public ValueBlockInterface $condition,
        public ValueBlockInterface $onNull
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s ?? %s',
            $this->condition->toTypescript($settings),
            $this->onNull->toTypescript($settings)
        );
    }
}
