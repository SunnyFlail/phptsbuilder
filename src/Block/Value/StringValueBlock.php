<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\Enum\StringDifferentiator;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class StringValueBlock implements ValueBlockInterface
{
    public function __construct(
        public string $value,
        public StringDifferentiator $differentiator = StringDifferentiator::SINGLE
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf('%1$s%2$s%1$s', $this->differentiator->value, $this->value);
    }
}
