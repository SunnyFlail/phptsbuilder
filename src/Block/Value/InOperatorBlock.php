<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class InOperatorBlock implements ValueBlockInterface
{
    public function __construct(
        public ValueBlockInterface $searched,
        public ValueBlockInterface $subject
    ) {}

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '(%s in %s)',
            $this->searched->toTypescript($settings),
            $this->subject->toTypescript($settings),
        );
    }
}
