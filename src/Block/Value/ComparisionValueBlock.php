<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\Enum\ComparisionOperator;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ComparisionValueBlock implements ValueBlockInterface
{
    public function __construct(
        public ValueBlockInterface $leftValue,
        public ValueBlockInterface $rightValue,
        public ComparisionOperator $operator
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s %s %s',
            $this->leftValue->toTypescript($settings),
            $this->operator->value,
            $this->rightValue->toTypescript($settings)
        );
    }
}
