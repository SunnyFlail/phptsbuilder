<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class NegationOperationBlock implements ValueBlockInterface
{
    public function __construct(public ValueBlockInterface $value) {}

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return '!'.$this->value->toTypescript($settings);
    }
}
