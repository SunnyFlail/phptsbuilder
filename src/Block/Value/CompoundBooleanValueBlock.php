<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\Enum\BooleanOperator;
use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<ValueBlockInterface>
 */
final readonly class CompoundBooleanValueBlock extends CollectiveBlock implements ValueBlockInterface
{
    use SpacingItemCallbackTrait;

    public function __construct(
        public BooleanOperator $operator,
        ValueBlockInterface ...$values
    ) {
        parent::__construct(...$values);
    }

    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s%s',
            $settings->newLineChar,
            $this->operator->value
        );
    }
}
