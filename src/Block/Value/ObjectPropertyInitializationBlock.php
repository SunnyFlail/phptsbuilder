<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\Reusable\ObjectPropertylikeBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;

final readonly class ObjectPropertyInitializationBlock extends ObjectPropertylikeBlock
{
    public function __construct(string $name, ValueBlockInterface $value)
    {
        parent::__construct($name, $value);
    }
}
