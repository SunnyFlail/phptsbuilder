<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class TypeOfOperatorBlock implements ValueBlockInterface
{
    public function __construct(public ValueBlockInterface $value)
    {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf('typeof %s', $this->value->toTypescript($settings));
    }
}
