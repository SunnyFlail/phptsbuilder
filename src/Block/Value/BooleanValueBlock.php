<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Value;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class BooleanValueBlock implements ValueBlockInterface
{
    public function __construct(public bool $value)
    {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return $this->value ? 'true' : 'false';
    }
}
