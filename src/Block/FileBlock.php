<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Constraint\CannotReturnValue;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueExportNames;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueImportNames;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Trait\IsEmptyLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\LineEndSemicolonItemCallbackTrait;
use SunnyFlail\PhpTsBuilder\Trait\StructurePropertySeparatorTrait;

/**
 * @template-implements FileBlockInterface<FileBodyStatementBlockInterface>
 *
 * @template-extends CollectiveBlock<FileBodyStatementBlockInterface>
 */
#[UniqueImportNames()]
#[UniqueExportNames()]
#[CannotReturnValue()]
final readonly class FileBlock extends CollectiveBlock implements FileBlockInterface
{
    use StructurePropertySeparatorTrait;
    use IsEmptyLineTrait;
    use LineEndSemicolonItemCallbackTrait;
    use IsATrait;

    public function __construct(FileBodyStatementBlockInterface ...$lines)
    {
        parent::__construct(...$lines);
    }

    public function getBodies(): iterable
    {
        foreach ($this->getItems() as $item) {
            if (
                $this->isA($item, BodyContainingBlockInterface::class)
                && !$this->isA($item, StructureBlockInterface::class)
            ) {
                yield from $item->getBodies();
            }
        }
    }

    public function getBodiesCount(): int
    {
        return iterator_count($this->getBodies());
    }
}
