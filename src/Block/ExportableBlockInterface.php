<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

interface ExportableBlockInterface extends BodyStatementBlockInterface, FileBodyStatementBlockInterface, UniqueNamedBlockInterface {}
