<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

/**
 * @template-covariant TItem of ControlBodyStatementBlockInterface
 *
 * @template-extends CollectiveBlockInterface<TItem>
 */
interface BodyBlockInterface extends CollectiveBlockInterface {}
