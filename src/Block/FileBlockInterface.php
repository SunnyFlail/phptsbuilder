<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

/**
 * @template-covariant TItem of FileBodyStatementBlockInterface
 *
 * @template-extends CollectiveBlockInterface<FileBodyStatementBlockInterface>
 * @template-extends BodyContainingBlockInterface<FileBodyStatementBlockInterface>
 */
interface FileBlockInterface extends CollectiveBlockInterface, BodyContainingBlockInterface {}
