<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Enum\IncrementationOperator;
use SunnyFlail\PhpTsBuilder\Block\Enum\IncrementationOrder;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class VariableValueIncrementationBlock implements ValueBlockInterface
{
    public function __construct(
        public VariableReferenceBlock $variable,
        public IncrementationOperator $operator,
        public IncrementationOrder $order = IncrementationOrder::LAST
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            match ($this->order) {
                IncrementationOrder::FIRST => '%2$s%1$s',
                IncrementationOrder::LAST => '%1$s%2$s',
            },
            $this->variable->toTypescript($settings),
            $this->operator->value
        );
    }
}
