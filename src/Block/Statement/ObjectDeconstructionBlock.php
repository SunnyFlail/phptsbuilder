<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

final readonly class ObjectDeconstructionBlock extends AbstractDeconstructionBlock
{
    protected function getStartCharacter(): string
    {
        return '{';
    }

    protected function getEndCharacter(): string
    {
        return '}';
    }
}
