<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Enum\VariableDeclarationType;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableDeclarationBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class AbstractDeconstructionBlock implements VariableDeclarationBlockInterface
{
    /**
     * @var array<string|DeconstructPropertyAsBlock>
     */
    public array $names;

    public function __construct(
        public VariableDeclarationType $declarationType,
        public ?ValueBlockInterface $source = null,
        string|DeconstructPropertyAsBlock ...$names
    ) {
        $this->names = $names;
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        foreach ($this->names as $name) {
            if ($name instanceof DeconstructPropertyAsBlock) {
                yield from $name->getNames();
            } else {
                yield $name;
            }
        }
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $names = implode(
            sprintf(',%s', $settings->newLineChar),
            array_map(
                function (string|DeconstructPropertyAsBlock $name) use ($settings): string {
                    if ($name instanceof DeconstructPropertyAsBlock) {
                        return $name->toTypescript($settings);
                    }

                    return $name;
                },
                $this->names
            )
        );

        return sprintf(
            '%s %s%s',
            $this->declarationType->value,
            implode(
                $settings->newLineChar,
                [
                    $this->getStartCharacter(),
                    $names,
                    $this->getEndCharacter(),
                ]
            ),
            !$this->source ? '' : sprintf(
                ' = ...%s',
                $this->source->toTypescript($settings)
            )
        );
    }

    abstract protected function getStartCharacter(): string;

    abstract protected function getEndCharacter(): string;
}
