<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<ValueBlockInterface>
 */
readonly class FunctionInvocationBlock extends CollectiveBlock implements ValueBlockInterface
{
    use CommaSeparatorWithNewLineTrait;
    use SpacingItemCallbackTrait;

    public function __construct(
        public string $name,
        ValueBlockInterface ...$arguments
    ) {
        parent::__construct(...$arguments);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%1$s(%2$s%3$s%2$s)',
            $this->name,
            $this->getItemCount() > 0 ? $settings->newLineChar : '',
            parent::toTypescript($settings)
        );
    }
}
