<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Enum\OperationType;
use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<ValueBlockInterface>
 */
final readonly class OperationBlock extends CollectiveBlock implements ValueBlockInterface
{
    use PassthroughItemCallbackTrait;

    public function __construct(
        public OperationType $type,
        ValueBlockInterface ...$values
    ) {
        parent::__construct(...$values);
    }

    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return sprintf(' %s ', $this->type->value);
    }
}
