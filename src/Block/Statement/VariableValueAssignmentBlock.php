<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Enum\AssignmentType;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class VariableValueAssignmentBlock implements ValueBlockInterface
{
    public function __construct(
        public string $name,
        public ValueBlockInterface $value,
        public AssignmentType $type = AssignmentType::ASSIGN
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s %s %s',
            $this->name,
            $this->type->value,
            $this->value->toTypescript($settings)
        );
    }
}
