<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Enum\VariableDeclarationType;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;

final readonly class ArrayDeconstructionBlock extends AbstractDeconstructionBlock
{
    public function __construct(
        VariableDeclarationType $declarationType,
        ValueBlockInterface $source,
        string ...$names
    ) {
        parent::__construct($declarationType, $source, ...$names);
    }

    protected function getStartCharacter(): string
    {
        return '[';
    }

    protected function getEndCharacter(): string
    {
        return ']';
    }
}
