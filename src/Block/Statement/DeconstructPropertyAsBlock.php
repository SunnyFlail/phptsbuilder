<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Reusable\ObjectPropertylikeBlock;

final readonly class DeconstructPropertyAsBlock extends ObjectPropertylikeBlock
{
    public function __construct(
        string $name,
        string $deconstructAs
    ) {
        parent::__construct($name, new VariableReferenceBlock($deconstructAs));
    }
}
