<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;

final readonly class ConstructorInvocationBlock extends FunctionInvocationBlock
{
    public function __construct(
        string $className,
        ValueBlockInterface ...$arguments
    ) {
        parent::__construct(sprintf('new %s', $className), ...$arguments);
    }
}
