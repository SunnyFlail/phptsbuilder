<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\Enum\VariableDeclarationType;
use SunnyFlail\PhpTsBuilder\Block\ExportableBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\StatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableDeclarationBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class VariableDeclarationBlock implements StatementBlockInterface, ExportableBlockInterface, VariableDeclarationBlockInterface
{
    public function __construct(
        public VariableDeclarationType $declarationType,
        public string $name,
        public ?ValueBlockInterface $value = null,
        public ?VariableTypeBlockInterface $variableType = null
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s %s%s%s',
            $this->declarationType->value,
            $this->name,
            !$this->variableType ? '' : sprintf(
                ': %s',
                $this->variableType->toTypescript($settings)
            ),
            !$this->value ? '' : sprintf(
                ' = %s',
                $this->value->toTypescript($settings)
            )
        );
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }
}
