<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Statement;

use SunnyFlail\PhpTsBuilder\Block\InvokableBlock;
use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<ValueBlockInterface>
 */
final readonly class InvokableInvocationBlock extends CollectiveBlock implements ValueBlockInterface
{
    use CommaSeparatorWithNewLineTrait;
    use SpacingItemCallbackTrait;

    public function __construct(
        public InvokableBlock $invokable,
        ValueBlockInterface ...$arguments
    ) {
        parent::__construct(...$arguments);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '(%1$s%2$s%1$s) (%3$s%4$s%3$s)',
            $settings->newLineChar,
            $this->invokable->toTypescript($settings),
            $settings->newLineChar,
            parent::toTypescript($settings)
        );
    }
}
