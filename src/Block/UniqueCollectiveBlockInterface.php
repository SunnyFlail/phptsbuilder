<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

/**
 * @template-covariant TItem of UniqueNamedBlockInterface
 *
 * @template-extends CollectiveBlockInterface<TItem>
 */
interface UniqueCollectiveBlockInterface extends CollectiveBlockInterface, UniqueNamedBlockInterface {}
