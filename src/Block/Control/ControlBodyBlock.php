<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control;

use SunnyFlail\PhpTsBuilder\Block\ControlBodyStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractBodyBlock;
use SunnyFlail\PhpTsBuilder\Constraint\AtLeastOneItem;
use SunnyFlail\PhpTsBuilder\Constraint\EndingStatementIsLastInBody;
use SunnyFlail\PhpTsBuilder\Constraint\SingleEndingStatement;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

/**
 * @template-extends AbstractBodyBlock<ControlBodyStatementBlockInterface>
 */
#[SingleEndingStatement()]
#[EndingStatementIsLastInBody()]
#[AtLeastOneItem()]
final readonly class ControlBodyBlock extends AbstractBodyBlock
{
    public function __construct(ControlBodyStatementBlockInterface ...$lines)
    {
        parent::__construct(...$lines);

        ValidatorFacade::validate($this);
    }
}
