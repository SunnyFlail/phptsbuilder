<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Import;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ImportDefault implements UniqueNamedBlockInterface
{
    public function __construct(public string $alias)
    {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return $this->alias;
    }

    public function getNames(): iterable
    {
        yield $this->alias;
    }
}
