<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Import;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractAliasedArgument;

final readonly class ImportItem extends AbstractAliasedArgument {}
