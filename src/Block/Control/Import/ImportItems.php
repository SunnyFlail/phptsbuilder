<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Import;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractInlineObjectBlock;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueStructurePropertyNames;

/**
 * @template-extends AbstractInlineObjectBlock<ImportItem>
 */
#[UniqueStructurePropertyNames()]
final readonly class ImportItems extends AbstractInlineObjectBlock implements ImportBodyInterface
{
    public function __construct(ImportItem ...$items)
    {
        parent::__construct(...$items);
    }
}
