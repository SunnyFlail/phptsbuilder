<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Import;

use SunnyFlail\PhpTsBuilder\Block\ImportBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ImportBlock implements ImportBlockInterface
{
    public function __construct(
        public string $moduleName,
        public ?ImportDefault $default = null,
        public ?ImportBodyInterface $body = null,
    ) {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        if ($this->default) {
            yield from $this->default->getNames();
        }

        if ($this->body) {
            yield from $this->body->getNames();
        }
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $importItems = [];

        if ($this->default) {
            $importItems[] = $this->default->toTypescript($settings);
        }

        if ($this->body) {
            $importItems[] = $this->body->toTypescript($settings);
        }

        return sprintf(
            'import %s"%s"',
            !$importItems ? '' : sprintf('%s from ', implode(', ', $importItems)),
            $this->moduleName
        );
    }
}
