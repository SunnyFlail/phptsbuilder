<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Import;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractAliasedArgument;

final readonly class ImportAll extends AbstractAliasedArgument implements ImportBodyInterface
{
    public function __construct(string $alias)
    {
        parent::__construct('*', $alias);
    }

    public function getNames(): iterable
    {
        return [];
    }
}
