<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control;

use SunnyFlail\PhpTsBuilder\Block\EndingStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\FileBodyStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;

final readonly class ThrowBlock extends AbstractControlBlock implements FileBodyStatementBlockInterface, EndingStatementBlockInterface
{
    public function __construct(ValueBlockInterface $value)
    {
        parent::__construct($value);
    }

    protected function getControlBlockName(): string
    {
        return 'throw';
    }
}
