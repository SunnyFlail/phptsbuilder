<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control;

use SunnyFlail\PhpTsBuilder\Block\EndingStatementBlockInterface;

final readonly class ReturnBlock extends AbstractControlBlock implements EndingStatementBlockInterface
{
    protected function getControlBlockName(): string
    {
        return 'return';
    }
}
