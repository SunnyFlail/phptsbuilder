<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Export;

use SunnyFlail\PhpTsBuilder\Block\ExportableBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ExportBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ExportBlock implements ExportBlockInterface
{
    public function __construct(public ExportableBlockInterface $block)
    {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        return $this->block->getNames();
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf('export %s', $this->block->toTypescript($settings));
    }
}
