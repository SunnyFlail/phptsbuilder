<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Export;

use SunnyFlail\PhpTsBuilder\Block\DefaultExportableBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ExportBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ExportDefaultBlock implements ExportBlockInterface
{
    public function __construct(public DefaultExportableBlockInterface $block)
    {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        yield 'default';
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf('export default %s', $this->block->toTypescript($settings));
    }
}
