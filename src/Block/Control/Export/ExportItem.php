<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Export;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractAliasedArgument;

final readonly class ExportItem extends AbstractAliasedArgument {}
