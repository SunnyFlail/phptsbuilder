<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Export;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractInlineObjectBlock;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueStructurePropertyNames;

/**
 * @template-extends AbstractInlineObjectBlock<ExportItem>
 */
#[UniqueStructurePropertyNames()]
final readonly class ExportItems extends AbstractInlineObjectBlock
{
    public function __construct(ExportItem ...$items)
    {
        parent::__construct(...$items);
    }
}
