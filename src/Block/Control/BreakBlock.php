<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control;

use SunnyFlail\PhpTsBuilder\Block\ControlBodyStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\EndingStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class BreakBlock implements ControlBodyStatementBlockInterface, EndingStatementBlockInterface
{
    public function __construct()
    {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return 'break';
    }
}
