<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\For;

final readonly class ForOfBlock extends AbstractForBlock
{
    protected function getLoopOperator(): string
    {
        return 'of';
    }
}
