<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\For;

final readonly class ForInBlock extends AbstractForBlock
{
    protected function getLoopOperator(): string
    {
        return 'in';
    }
}
