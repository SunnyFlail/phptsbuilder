<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\For;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\LoopBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\StatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableDeclarationBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlockToTypescriptTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ForBlock implements LoopBlockInterface
{
    use ControlBlockToTypescriptTrait;

    public function __construct(
        public ?VariableDeclarationBlockInterface $variableInitialization,
        public ValueBlockInterface $condition,
        public ?StatementBlockInterface $callback,
        public ControlBodyBlock $body
    ) {
        ValidatorFacade::validate($this);
    }

    public function getBodies(): iterable
    {
        yield $this->body;
    }

    public function getBodiesCount(): int
    {
        return 1;
    }

    protected function buildStartBlock(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            'for (%s;%s;%s) {',
            $this->variableInitialization?->toTypescript($settings) ?? '',
            $this->condition->toTypescript($settings),
            $this->callback?->toTypescript($settings) ?? ''
        );
    }

    protected function buildEndBlock(TypescriptCodeSettings $settings): string
    {
        return '}';
    }
}
