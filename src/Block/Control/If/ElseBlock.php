<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\If;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Settings\MockSettings;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlockToTypescriptTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ElseBlock implements ElseBlockInterface
{
    use ControlBlockToTypescriptTrait;

    public function __construct(public ControlBodyBlock $body)
    {
        ValidatorFacade::validate($this);
    }

    public function getBodies(): iterable
    {
        yield $this->body;
    }

    public function getBodiesCount(): int
    {
        return 1;
    }

    public function getNames(): iterable
    {
        yield $this->buildStartBlock(MockSettings::getMockSettings());
    }

    protected function buildStartBlock(TypescriptCodeSettings $settings): string
    {
        return 'else {';
    }

    protected function buildEndBlock(TypescriptCodeSettings $settings): string
    {
        return '}';
    }
}
