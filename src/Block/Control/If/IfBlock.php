<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\If;

use SunnyFlail\PhpTsBuilder\Block\ConditionalBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AtMostOneElseWithoutCondition;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\BuildControlBlockDeclarationTrait;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlocksSeparatorTrait;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlockToTypescriptTrait;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<ElseBlockInterface>
 */
#[AtMostOneElseWithoutCondition()]
final readonly class IfBlock extends CollectiveBlock implements ConditionalBlockInterface
{
    use PassthroughItemCallbackTrait;
    use ControlBlocksSeparatorTrait;
    use ControlBlockToTypescriptTrait;
    use BuildControlBlockDeclarationTrait;

    public function __construct(
        public ValueBlockInterface $condition,
        public ControlBodyBlock $body,
        ElseBlockInterface ...$elseCases
    ) {
        parent::__construct(...$elseCases);
    }

    public function getBodies(): iterable
    {
        yield $this->body;

        foreach ($this->getItems() as $else) {
            yield from $else->getBodies();
        }
    }

    public function getBodiesCount(): int
    {
        return iterator_count($this->getBodies());
    }

    protected function buildStartBlock(TypescriptCodeSettings $settings): string
    {
        return $this->buildControlBlockDeclaration(
            $settings,
            'if',
            $this->condition,
            ' {'
        );
    }

    protected function buildEndBlock(TypescriptCodeSettings $settings): string
    {
        if ($this->getItemCount() > 0) {
            return '} '.parent::toTypescript($settings);
        }

        return '}';
    }
}
