<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Switch;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class DefaultCaseBlock extends AbstractCaseBlock
{
    protected function buildCase(TypescriptCodeSettings $settings): string
    {
        return 'default:';
    }
}
