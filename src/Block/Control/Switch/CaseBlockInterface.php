<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Switch;

use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;

/**
 * @template-extends BodyContainingBlockInterface<ControlBodyBlock>
 */
interface CaseBlockInterface extends BodyContainingBlockInterface, UniqueNamedBlockInterface {}
