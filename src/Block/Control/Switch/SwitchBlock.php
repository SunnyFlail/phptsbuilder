<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Switch;

use SunnyFlail\PhpTsBuilder\Block\ConditionalBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Reusable\UniqueCollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueSwitchConditions;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\IsMultilineTrait;
use SunnyFlail\PhpTsBuilder\Trait\NewLineSeparatorTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends UniqueCollectiveBlock<CaseBlockInterface>
 */
#[UniqueSwitchConditions()]
final readonly class SwitchBlock extends UniqueCollectiveBlock implements ConditionalBlockInterface
{
    use IsMultilineTrait;
    use SpacingItemCallbackTrait;
    use NewLineSeparatorTrait;

    public function __construct(
        public ValueBlockInterface $condition,
        CaseBlockInterface ...$cases
    ) {
        parent::__construct(...$cases);
    }

    public function getBodies(): iterable
    {
        foreach ($this->getItems() as $case) {
            yield from $case->getBodies();
        }
    }

    public function getBodiesCount(): int
    {
        return iterator_count($this->getBodies());
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $declaration = 'switch (%1$s%2$s%1$s) {';
        $condition = $this->addSpacing($this->condition, $settings);
        $addNewLine = $this->isMultiline(
            $settings,
            $declaration,
            '%1$s%2$s%1$s',
            $condition
        );
        $declaration = sprintf(
            $declaration,
            $addNewLine ? $settings->newLineChar : '',
            $addNewLine ? $condition : trim($condition)
        );

        return implode(
            $settings->newLineChar,
            [
                $declaration,
                parent::toTypescript($settings),
                '}',
            ]
        );
    }
}
