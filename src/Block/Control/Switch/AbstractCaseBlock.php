<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Switch;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Settings\MockSettings;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class AbstractCaseBlock implements CaseBlockInterface
{
    use SpacingTrait;

    public function __construct(public ?ControlBodyBlock $body = null)
    {
        ValidatorFacade::validate($this);
    }

    public function getBodies(): iterable
    {
        if ($this->body) {
            yield $this->body;
        }
    }

    public function getBodiesCount(): int
    {
        return $this->body ? 1 : 0;
    }

    public function getNames(): iterable
    {
        yield $this->buildCase(MockSettings::getMockSettings());
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $lines = [$this->buildCase($settings)];

        if ($this->body) {
            $lines[] = $this->addSpacing($this->body, $settings);
        }

        return implode(
            $settings->newLineChar,
            $lines
        );
    }

    abstract protected function buildCase(TypescriptCodeSettings $settings): string;
}
