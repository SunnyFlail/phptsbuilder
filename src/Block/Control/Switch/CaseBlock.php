<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Switch;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class CaseBlock extends AbstractCaseBlock
{
    public function __construct(
        public ValueBlockInterface $condition,
        ?ControlBodyBlock $body = null
    ) {
        parent::__construct($body);
    }

    protected function buildCase(TypescriptCodeSettings $settings): string
    {
        return sprintf('case %s:', $this->condition->toTypescript($settings));
    }
}
