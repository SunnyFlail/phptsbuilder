<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control;

use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;

final readonly class YieldBlock extends AbstractControlBlock
{
    public function __construct(
        ValueBlockInterface $value,
        public bool $fromGenerator = false
    ) {
        parent::__construct($value);
    }

    protected function getControlBlockName(): string
    {
        return sprintf('yield%s', $this->fromGenerator ? '*' : '');
    }
}
