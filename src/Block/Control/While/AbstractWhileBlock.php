<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\While;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\LoopBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlockToTypescriptTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class AbstractWhileBlock implements LoopBlockInterface
{
    use SpacingTrait;
    use ControlBlockToTypescriptTrait;

    public function __construct(
        public ValueBlockInterface $condition,
        public ControlBodyBlock $body
    ) {
        ValidatorFacade::validate($this);
    }

    public function getBodies(): iterable
    {
        yield $this->body;
    }

    public function getBodiesCount(): int
    {
        return 1;
    }
}
