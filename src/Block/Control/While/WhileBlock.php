<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\While;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\BuildControlBlockDeclarationTrait;

final readonly class WhileBlock extends AbstractWhileBlock
{
    use BuildControlBlockDeclarationTrait;

    protected function buildStartBlock(TypescriptCodeSettings $settings): string
    {
        return $this->buildControlBlockDeclaration(
            $settings,
            'while',
            $this->condition,
            ' {'
        );
    }

    protected function buildEndBlock(TypescriptCodeSettings $settings): string
    {
        return '}';
    }
}
