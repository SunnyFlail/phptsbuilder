<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control;

use SunnyFlail\PhpTsBuilder\Block\BodyStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class AbstractControlBlock implements BodyStatementBlockInterface
{
    public function __construct(public ?ValueBlockInterface $value = null)
    {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s%s',
            $this->getControlBlockName(),
            !$this->value ? '' : ' '.$this->value->toTypescript($settings),
        );
    }

    abstract protected function getControlBlockName(): string;
}
