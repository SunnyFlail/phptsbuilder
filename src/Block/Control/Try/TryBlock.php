<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Try;

use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\StatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlockToTypescriptTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

/**
 * @template-implements BodyContainingBlockInterface<ControlBodyBlock>
 */
final readonly class TryBlock implements BodyContainingBlockInterface, StatementBlockInterface
{
    use ControlBlockToTypescriptTrait;

    public function __construct(
        public ControlBodyBlock $body,
        public CatchBlock $catchBlock
    ) {
        ValidatorFacade::validate($this);
    }

    public function getBodies(): iterable
    {
        yield $this->body;

        yield from $this->catchBlock->getBodies();
    }

    public function getBodiesCount(): int
    {
        return iterator_count($this->getBodies());
    }

    protected function buildStartBlock(TypescriptCodeSettings $settings): string
    {
        return 'try {';
    }

    protected function buildEndBlock(TypescriptCodeSettings $settings): string
    {
        return '} '.$this->catchBlock->toTypescript($settings);
    }
}
