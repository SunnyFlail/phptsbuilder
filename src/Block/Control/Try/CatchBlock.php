<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Control\Try;

use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\Statement\VariableReferenceBlock;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\ControlBlockToTypescriptTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

/**
 * @template-implements BodyContainingBlockInterface<ControlBodyBlock>
 */
final readonly class CatchBlock implements BodyContainingBlockInterface
{
    use ControlBlockToTypescriptTrait;

    public function __construct(
        public VariableReferenceBlock $caughtException,
        public ControlBodyBlock $body
    ) {
        ValidatorFacade::validate($this);
    }

    public function getBodies(): iterable
    {
        yield $this->body;
    }

    public function getBodiesCount(): int
    {
        return 1;
    }

    protected function buildStartBlock(TypescriptCodeSettings $settings): string
    {
        return sprintf('catch (%s) {', $this->caughtException->toTypescript($settings));
    }

    protected function buildEndBlock(TypescriptCodeSettings $settings): string
    {
        return '}';
    }
}
