<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

interface ValueBlockInterface extends StatementBlockInterface, DefaultExportableBlockInterface {}
