<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

interface StructureReferenceBlockInterface extends UniqueNamedBlockInterface {}
