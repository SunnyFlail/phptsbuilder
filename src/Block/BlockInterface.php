<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

interface BlockInterface
{
    public function toTypescript(TypescriptCodeSettings $settings): string;
}
