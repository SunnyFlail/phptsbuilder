<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class MultilineCommentBlock implements BlockInterface
{
    /**
     * @var string[]
     */
    public array $lines;

    public function __construct(string ...$lines)
    {
        $this->lines = $lines;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $lines = ['/**'];

        foreach ($this->lines as $line) {
            $lines[] = ' * '.$line;
        }

        $lines[] = ' */';

        return implode($settings->newLineChar, $lines);
    }
}
