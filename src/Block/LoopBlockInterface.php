<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;

/**
 * @template-extends BodyContainingBlockInterface<ControlBodyBlock>
 */
interface LoopBlockInterface extends BodyContainingBlockInterface, StatementBlockInterface {}
