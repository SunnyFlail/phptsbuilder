<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

interface UniqueNamedBlockInterface extends BlockInterface
{
    /**
     * @return iterable<string>
     */
    public function getNames(): iterable;
}
