<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ControlBodyStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Trait\LineEndSemicolonItemCallbackTrait;
use SunnyFlail\PhpTsBuilder\Trait\NewLineSeparatorTrait;

/**
 * @template-covariant TItem of ControlBodyStatementBlockInterface
 *
 * @template-extends CollectiveBlock<TItem>
 *
 * @template-implements BodyBlockInterface<TItem>
 */
abstract readonly class AbstractBodyBlock extends CollectiveBlock implements BodyBlockInterface
{
    use NewLineSeparatorTrait;
    use LineEndSemicolonItemCallbackTrait;

    /**
     * @param TItem[] $lines
     */
    public function __construct(BlockInterface ...$lines)
    {
        parent::__construct(...$lines);
    }
}
