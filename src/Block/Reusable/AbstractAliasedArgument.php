<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class AbstractAliasedArgument implements UniqueNamedBlockInterface
{
    public function __construct(
        public string $itemName,
        public ?string $alias = null
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s%s',
            $this->itemName,
            !$this->alias ? '' : sprintf(' as %s', $this->alias)
        );
    }

    public function getNames(): iterable
    {
        yield $this->alias ?: $this->itemName;
    }
}
