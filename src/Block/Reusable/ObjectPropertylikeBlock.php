<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Enum\AccessModifier;
use SunnyFlail\PhpTsBuilder\Block\StructurePropertyBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class ObjectPropertylikeBlock implements StructurePropertyBlockInterface
{
    public function __construct(
        public string $name,
        public BlockInterface $value,
        public ?AccessModifier $accessModifier = null,
        public bool $static = false,
        public bool $readonly = false,
    ) {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s: %s',
            $this->buildPropertyName(),
            $this->value->toTypescript($settings)
        );
    }

    protected function buildPropertyName(): string
    {
        return sprintf(
            '%s%s%s%s',
            !$this->accessModifier ? '' : sprintf('%s ', $this->accessModifier->value),
            !$this->static ? '' : 'static ',
            !$this->readonly ? '' : 'readonly ',
            $this->name,
        );
    }
}
