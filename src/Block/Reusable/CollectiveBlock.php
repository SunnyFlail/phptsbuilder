<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

/**
 * @template-covariant TItem of BlockInterface
 *
 * @template-implements CollectiveBlockInterface<TItem>
 */
abstract readonly class CollectiveBlock implements CollectiveBlockInterface
{
    /**
     * @var array<int,TItem>
     */
    protected array $items;

    /**
     * @param TItem $items
     */
    public function __construct(BlockInterface ...$items)
    {
        $this->items = $items;
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $items = [];

        foreach ($this->items as $item) {
            $items[] = $this->itemCallback($item, $settings);
        }

        if (!$items) {
            return '';
        }

        return implode(
            $this->buildSeparator($settings),
            $items
        );
    }

    final public function getItemCount(): int
    {
        return count($this->items);
    }

    final public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param TItem $item
     */
    abstract protected function itemCallback(BlockInterface $item, TypescriptCodeSettings $settings): string;

    abstract protected function buildSeparator(TypescriptCodeSettings $settings): string;
}
