<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\StructurePropertiesContainerInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;

/**
 * @template-covariant TItem of UniqueNamedBlockInterface
 *
 * @template-extends UniqueCollectiveBlock<TItem>
 */
abstract readonly class AbstractInlineObjectBlock extends UniqueCollectiveBlock implements StructurePropertiesContainerInterface
{
    use PassthroughItemCallbackTrait;

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return implode(
            ' ',
            [
                '{',
                parent::toTypescript($settings),
                '}',
            ]
        );
    }

    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return ', ';
    }
}
