<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<VariableTypeBlockInterface>
 */
abstract readonly class AbstractTemplatedTypeBlock extends CollectiveBlock implements UniqueNamedBlockInterface
{
    use PassthroughItemCallbackTrait;

    public function __construct(
        public string $structureName,
        VariableTypeBlockInterface ...$templates
    ) {
        parent::__construct(...$templates);
    }

    public function getNames(): iterable
    {
        yield $this->structureName;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s%s',
            $this->structureName,
            !$this->getItemCount() ? '' : sprintf('<%s>', parent::toTypescript($settings))
        );
    }

    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return ',';
    }
}
