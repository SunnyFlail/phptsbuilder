<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Reusable;

use SunnyFlail\PhpTsBuilder\Block\UniqueCollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;

/**
 * @template-covariant TItem of UniqueNamedBlockInterface
 *
 * @template-extends CollectiveBlock<TItem>
 *
 * @template-implements UniqueCollectiveBlockInterface<TItem>
 */
abstract readonly class UniqueCollectiveBlock extends CollectiveBlock implements UniqueCollectiveBlockInterface
{
    /**
     * @param TItem $items
     */
    public function __construct(UniqueNamedBlockInterface ...$items)
    {
        parent::__construct(...$items);
    }

    public function getNames(): iterable
    {
        foreach ($this->getItems() as $item) {
            yield from $item->getNames();
        }
    }
}
