<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block;

use SunnyFlail\PhpTsBuilder\Block\Enum\CommentType;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class TsIgnoreBlock implements StatementBlockInterface, ValueBlockInterface
{
    public function __construct(
        public StatementBlockInterface|ValueBlockInterface $ignoredBlock,
        public CommentType $commentType = CommentType::SINGLE_LINE
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $comment = match ($this->commentType) {
            CommentType::SINGLE_LINE => '// @ts-ignore',
            CommentType::MULTI_LINE => sprintf(
                '/**%1$s * @ts-ignore%1$s */',
                $settings->newLineChar
            )
        };

        return sprintf(
            '%s%s%s',
            $comment,
            $settings->newLineChar,
            $this->ignoredBlock->toTypescript($settings)
        );
    }
}
