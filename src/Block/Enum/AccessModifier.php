<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum AccessModifier: string
{
    case PUBLIC = 'public';
    case PROTECTED = 'protected';
    case PRIVATE = 'private';
}
