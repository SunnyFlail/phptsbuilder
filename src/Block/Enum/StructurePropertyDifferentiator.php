<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum StructurePropertyDifferentiator: string
{
    case SEMICOLON = ';';
    case COMMA = ',';
}
