<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum BooleanOperator: string
{
    case AND = '&&';
    case OR = '||';
}
