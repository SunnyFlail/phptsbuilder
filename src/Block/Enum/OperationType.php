<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum OperationType: string
{
    case ADD = '+';
    case SUBTRACT = '-';
    case MULTIPLICATE = '*';
    case DIVIDE = '/';
    case EXPONENTATE = '**';
    case AND = '&&';
    case OR = '||';
    case BIT_AND = '&';
    case BIT_OR = '|';
    case BIT_XOR = '^';
}
