<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum VariableDeclarationType: string
{
    case VAR = 'var';
    case LET = 'let';
    case CONST = 'const';
}
