<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum IncrementationOrder
{
    case LAST;
    case FIRST;
}
