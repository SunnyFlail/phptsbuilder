<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum IncrementationOperator: string
{
    case INCREMENT = '++';
    case DECREMENT = '--';
}
