<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum AssignmentType: string
{
    case ASSIGN = '=';
    case ADD = '+=';
    case SUBTRACT = '-=';
    case MULTIPLICATE = '*=';
    case DIVIDE = '/=';
    case EXPONENTATE = '**=';
    case NULLISH_COALESCING = '??=';
    case OR = '|=';
    case AND = '&=';
    case LOGICAL_OR = '||=';
    case LOGICAL_AND = '&&=';
    case LEFT_SHIFT = '<<=';
    case RIGHT_SHIFT = '>>=';
    case REMAINDER = '%=';
}
