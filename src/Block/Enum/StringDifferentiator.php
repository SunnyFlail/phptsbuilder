<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum StringDifferentiator: string
{
    case SINGLE = "'";
    case DOUBLE = '"';
    case TEMPLATE = '`';
}
