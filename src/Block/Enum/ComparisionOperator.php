<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum ComparisionOperator: string
{
    case EQUALS_STRICT = '===';
    case EQUALS = '==';
    case INEQUAL_STRICT = '!==';
    case INEQUAL = '!=';
    case GREATER_THAN = '>';
    case LESSER_THAN = '<';
    case GREATER_OR_EQUAL = '>=';
    case LESSER_OR_EQUAL = '<=';
}
