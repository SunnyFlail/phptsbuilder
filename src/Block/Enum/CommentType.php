<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Enum;

enum CommentType
{
    case SINGLE_LINE;
    case MULTI_LINE;
}
