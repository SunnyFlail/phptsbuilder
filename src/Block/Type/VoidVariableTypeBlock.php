<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class VoidVariableTypeBlock implements VariableTypeBlockInterface
{
    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return 'void';
    }
}
