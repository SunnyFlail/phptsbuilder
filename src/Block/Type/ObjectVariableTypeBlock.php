<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;
use SunnyFlail\PhpTsBuilder\Trait\StructurePropertyDifferentiatorTrait;

/**
 * @template-extends CollectiveBlock<ObjectVariableTypePropertyBlock|ArrayDelimitedObjectPropertyBlock>
 */
final readonly class ObjectVariableTypeBlock extends CollectiveBlock implements VariableTypeBlockInterface
{
    use StructurePropertyDifferentiatorTrait;
    use SpacingItemCallbackTrait;

    public function __construct(ObjectVariableTypePropertyBlock|ArrayDelimitedObjectPropertyBlock ...$properties)
    {
        parent::__construct(...$properties);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return implode(
            $settings->newLineChar,
            [
                '{',
                parent::toTypescript($settings),
                '}',
            ]
        );
    }
}
