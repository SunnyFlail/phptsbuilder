<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class ArrayDelimitedObjectPropertyBlock implements BlockInterface
{
    public function __construct(
        public string $keyName,
        public VariableTypeBlockInterface $keyType,
        public VariableTypeBlockInterface $valueType
    ) {}

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '[%s: %s]: %s',
            $this->keyName,
            $this->keyType->toTypescript($settings),
            $this->valueType->toTypescript($settings),
        );
    }
}
