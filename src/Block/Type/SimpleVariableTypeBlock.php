<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class SimpleVariableTypeBlock implements VariableTypeBlockInterface, UniqueNamedBlockInterface
{
    public function __construct(public string $type)
    {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        yield $this->type;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return $this->type;
    }
}
