<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\Structure\Function\DeltaParametersBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class DeltaFunctionVariableTypeBlock implements VariableTypeBlockInterface
{
    use SpacingTrait;

    public function __construct(
        public ?DeltaParametersBlock $parameters,
        public VariableTypeBlockInterface $returnType
    ) {
        ValidatorFacade::validate($this);
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $multipleParameters = $this->parameters
            && in_array($this->parameters->getItemCount(), [0, 1])
        ;

        return sprintf(
            '(%1$s%2$s%1$s) => %3$s',
            !$multipleParameters ? '' : $settings->newLineChar,
            !$multipleParameters ? '' : $this->addSpacing($this->parameters, $settings),
            $this->returnType->toTypescript($settings)
        );
    }
}
