<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractTemplatedTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueTemplateTypeNames;

#[UniqueTemplateTypeNames()]
final readonly class ArrayVariableTypeBlock extends AbstractTemplatedTypeBlock implements VariableTypeBlockInterface
{
    public function __construct(VariableTypeBlockInterface $type)
    {
        parent::__construct('Array', $type);
    }
}
