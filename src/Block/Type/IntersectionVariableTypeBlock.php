<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;

/**
 * @template-extends CollectiveBlock<VariableTypeBlockInterface>
 */
final readonly class IntersectionVariableTypeBlock extends CollectiveBlock implements VariableTypeBlockInterface
{
    use PassthroughItemCallbackTrait;

    public function __construct(VariableTypeBlockInterface ...$types)
    {
        parent::__construct(...$types);
    }

    protected function buildSeparator(TypescriptCodeSettings $settings): string
    {
        return '|';
    }
}
