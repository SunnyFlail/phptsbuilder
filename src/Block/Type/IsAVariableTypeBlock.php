<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\Statement\VariableReferenceBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class IsAVariableTypeBlock implements VariableTypeBlockInterface
{
    public function __construct(
        public VariableReferenceBlock $referencedVariable,
        public VariableTypeBlockInterface $type
    ) {}

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s is %s',
            $this->referencedVariable->toTypescript($settings),
            $this->type->toTypescript($settings)
        );
    }
}
