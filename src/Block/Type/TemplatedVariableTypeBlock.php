<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractTemplatedTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueTemplateTypeNames;

#[UniqueTemplateTypeNames()]
final readonly class TemplatedVariableTypeBlock extends AbstractTemplatedTypeBlock implements VariableTypeBlockInterface {}
