<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class KeyOfVariableTypeBlock implements VariableTypeBlockInterface
{
    public function __construct(public VariableTypeBlockInterface $type) {}

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf('keyof %s', $this->type->toTypescript($settings));
    }
}
