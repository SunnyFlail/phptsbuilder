<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Type;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

final readonly class TemplateExtendsTypeBlock implements VariableTypeBlockInterface, UniqueNamedBlockInterface
{
    public function __construct(
        public string $name,
        public VariableTypeBlockInterface $extendedType
    ) {}

    /**
     * @return iterable<string>
     */
    public function getNames(): iterable
    {
        yield $this->name;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s extends %s',
            $this->name,
            $this->extendedType->toTypescript($settings)
        );
    }
}
