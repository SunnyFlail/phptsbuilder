<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Interface;

use SunnyFlail\PhpTsBuilder\Block\Reusable\ObjectPropertylikeBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;

final readonly class InterfacePropertyBlock extends ObjectPropertylikeBlock implements InterfacePropertyBlockInterface
{
    public function __construct(
        string $name,
        VariableTypeBlockInterface $type,
        bool $readonly = false
    ) {
        parent::__construct($name, $type, null, false, $readonly);
    }
}
