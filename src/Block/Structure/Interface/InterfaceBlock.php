<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Interface;

use SunnyFlail\PhpTsBuilder\Block\DefaultExportableBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\Reusable\UniqueCollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\StructureBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueStructurePropertyNames;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;
use SunnyFlail\PhpTsBuilder\Trait\StructurePropertyDifferentiatorTrait;

/**
 * @template-extends UniqueCollectiveBlock<InterfacePropertyBlockInterface>
 */
#[UniqueStructurePropertyNames()]
final readonly class InterfaceBlock extends UniqueCollectiveBlock implements StructureBlockInterface, DefaultExportableBlockInterface, MultilineBlockInterface
{
    use SpacingTrait;
    use StructurePropertyDifferentiatorTrait;
    use PassthroughItemCallbackTrait;

    public function __construct(
        public string|InterfaceNameBlock $name,
        public ?ExtendedInterfacesBlock $extendedInterfaces = null,
        public ?MultilineCommentBlock $comment = null,
        InterfacePropertyBlockInterface ...$properties
    ) {
        parent::__construct(...$properties);
    }

    public function getNames(): iterable
    {
        if (!is_string($this->name)) {
            return $this->name->getNames();
        }

        yield $this->name;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $lines = [];

        if ($this->comment) {
            $lines[] = $this->addSpacing($this->comment, $settings);
        }

        $lines[] = sprintf(
            'interface %s%s',
            is_string($this->name) ?
                $this->name :
                $this->name->toTypescript($settings),
            !$this->extendedInterfaces ?
                '' :
                sprintf(
                    ' extends %s',
                    $this->extendedInterfaces->toTypescript($settings)
                )
        );
        $lines[] = '{';
        $lines[] = parent::toTypescript($settings);
        $lines[] = '}';

        return implode($settings->newLineChar, $lines);
    }
}
