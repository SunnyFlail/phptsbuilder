<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Interface;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractTemplatedTypeBlock;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueTemplateTypeNames;

#[UniqueTemplateTypeNames()]
final readonly class InterfaceNameBlock extends AbstractTemplatedTypeBlock {}
