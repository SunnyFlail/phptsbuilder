<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Interface;

use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\AbstractFunctionBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\ParametersBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;

final readonly class InterfaceMethodBlock extends AbstractFunctionBlock implements InterfacePropertyBlockInterface
{
    public function __construct(
        public string $name,
        ?ParametersBlock $parameters,
        VariableTypeBlockInterface $returnType,
        bool $async = false,
        ?MultilineCommentBlock $comment = null
    ) {
        parent::__construct(
            parameters: $parameters,
            returnType: $returnType,
            async: $async,
            comment: $comment,
            addBody: false
        );
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }

    protected function buildFunctionName(): string
    {
        return $this->name;
    }
}
