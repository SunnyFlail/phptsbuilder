<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Interface;

use SunnyFlail\PhpTsBuilder\Block\StructurePropertyBlockInterface;

interface InterfacePropertyBlockInterface extends StructurePropertyBlockInterface {}
