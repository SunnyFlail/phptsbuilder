<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Constraint\UniqueFunctionParameterNames;

/**
 * @template-extends AbstractParametersBlock<ParameterBlock>
 */
#[UniqueFunctionParameterNames()]
final readonly class ParametersBlock extends AbstractParametersBlock
{
    public function __construct(ParameterBlock ...$parameters)
    {
        parent::__construct(...$parameters);
    }
}
