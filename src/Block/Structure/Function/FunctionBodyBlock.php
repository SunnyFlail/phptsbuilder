<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\BodyStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractBodyBlock;
use SunnyFlail\PhpTsBuilder\Constraint\EndingStatementIsLastInBody;
use SunnyFlail\PhpTsBuilder\Constraint\SingleEndingStatement;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

/**
 * @template-extends AbstractBodyBlock<BodyStatementBlockInterface>
 */
#[SingleEndingStatement()]
#[EndingStatementIsLastInBody()]
final readonly class FunctionBodyBlock extends AbstractBodyBlock implements FunctionBodyBlockInterface
{
    public function __construct(BodyStatementBlockInterface ...$lines)
    {
        parent::__construct(...$lines);

        ValidatorFacade::validate($this);
    }
}
