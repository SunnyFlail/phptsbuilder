<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;

/**
 * @template-extends BodyContainingBlockInterface<FunctionBodyBlockInterface>
 */
interface FunctionBlockInterface extends BodyContainingBlockInterface
{
    /**
     * @return array<FunctionParameterBlockInterface>
     */
    public function getParameters(): array;

    public function getReturnType(): ?VariableTypeBlockInterface;

    public function isGenerator(): bool;

    public function isAsync(): bool;
}
