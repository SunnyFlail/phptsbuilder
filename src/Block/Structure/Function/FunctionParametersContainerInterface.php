<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\UniqueCollectiveBlockInterface;

/**
 * @template-extends UniqueCollectiveBlockInterface<FunctionParameterBlockInterface>
 */
interface FunctionParametersContainerInterface extends UniqueCollectiveBlockInterface {}
