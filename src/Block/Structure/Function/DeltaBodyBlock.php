<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ReturnBlock;
use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\StatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AtLeastOneItem;
use SunnyFlail\PhpTsBuilder\Constraint\CallbackConfiguredConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\EndingStatementIsLastInBody;
use SunnyFlail\PhpTsBuilder\Constraint\SingleEndingStatement;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\NewLineSeparatorTrait;
use SunnyFlail\PhpTsBuilder\Violation\ReturnInOneLineDeltaFunction;

/**
 * @template-extends CollectiveBlock<StatementBlockInterface>
 */
#[SingleEndingStatement()]
#[EndingStatementIsLastInBody()]
#[AtLeastOneItem()]
#[CallbackConfiguredConstraint(
    function (DeltaBodyBlock $block): iterable {
        if (1 === $block->getItemCount() && $block->getItems()[0] instanceof ReturnBlock) {
            yield new ReturnInOneLineDeltaFunction();
        }
    }
)]
final readonly class DeltaBodyBlock extends CollectiveBlock implements ValueBlockInterface, FunctionBodyBlockInterface
{
    use NewLineSeparatorTrait;

    public function __construct(StatementBlockInterface ...$lines)
    {
        parent::__construct(...$lines);
    }

    protected function itemCallback(BlockInterface $item, TypescriptCodeSettings $settings): string
    {
        $line = $item->toTypescript($settings);

        if (
            in_array($this->getItemCount(), [0, 1])
            || !$line
            || $line === $settings->newLineChar
        ) {
            return $line;
        }

        return sprintf('%s;', $line);
    }
}
