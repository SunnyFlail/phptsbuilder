<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\InvokableBlock;
use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AwaitInsideAsync;
use SunnyFlail\PhpTsBuilder\Constraint\YieldInsideGenerator;

#[AwaitInsideAsync()]
#[YieldInsideGenerator()]
final readonly class AnonymousFunctionBlock extends AbstractFunctionBlock implements InvokableBlock, MultilineBlockInterface
{
    public function __construct(
        ?FunctionParametersContainerInterface $parameters = null,
        ?VariableTypeBlockInterface $returnType = null,
        ?FunctionBodyBlock $body = null,
        bool $async = false,
        bool $generator = false,
    ) {
        parent::__construct($parameters, $returnType, $body, $async, $generator);
    }

    protected function buildFunctionName(): string
    {
        return sprintf(
            '%sfunction%s',
            !$this->async ? '' : 'async ',
            !$this->generator ? '' : '*',
        );
    }
}
