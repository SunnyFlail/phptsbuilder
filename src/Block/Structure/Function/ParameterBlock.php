<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\Structure\Class\ConstructorParameterBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class ParameterBlock implements FunctionParameterBlockInterface, ConstructorParameterBlockInterface
{
    public function __construct(
        public string $name,
        public VariableTypeBlockInterface $type
    ) {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf('%s: %s', $this->name, $this->type->toTypescript($settings));
    }
}
