<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\InvokableBlock;
use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\StructureBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AwaitInsideAsync;
use SunnyFlail\PhpTsBuilder\Constraint\ReturnsValueIfSpecified;
use SunnyFlail\PhpTsBuilder\Constraint\YieldInsideGenerator;

#[AwaitInsideAsync()]
#[YieldInsideGenerator()]
#[ReturnsValueIfSpecified()]
final readonly class FunctionBlock extends AbstractFunctionBlock implements StructureBlockInterface, InvokableBlock, MultilineBlockInterface
{
    public function __construct(
        public string $name,
        ?ParametersBlock $parameters,
        ?VariableTypeBlockInterface $returnType,
        ?FunctionBodyBlock $body,
        bool $async = false,
        bool $generator = false,
        ?MultilineCommentBlock $comment = null
    ) {
        parent::__construct(
            $parameters,
            $returnType,
            $body,
            $async,
            $generator,
            $comment
        );
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }

    protected function buildFunctionName(): string
    {
        return sprintf(
            '%sfunction%s %s',
            !$this->async ? '' : 'async ',
            !$this->generator ? '' : '*',
            $this->name
        );
    }
}
