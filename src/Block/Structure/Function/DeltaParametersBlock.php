<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueFunctionParameterNames;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;

/**
 * @template-extends AbstractParametersBlock<ParameterBlock>
 */
#[UniqueFunctionParameterNames()]
final readonly class DeltaParametersBlock extends AbstractParametersBlock
{
    use CommaSeparatorWithNewLineTrait;

    public function __construct(ParameterBlock ...$parameters)
    {
        parent::__construct(...$parameters);
    }

    protected function itemCallback(BlockInterface $item, TypescriptCodeSettings $settings): string
    {
        if (in_array($this->getItemCount(), [0, 1])) {
            return $item->toTypescript($settings);
        }

        return parent::itemCallback($item, $settings);
    }
}
