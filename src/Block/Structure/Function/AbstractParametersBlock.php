<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\Reusable\UniqueCollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;

/**
 * @template-covariant TParameter of UniqueNamedBlockInterface
 *
 * @template-extends UniqueCollectiveBlock<TParameter>
 */
abstract readonly class AbstractParametersBlock extends UniqueCollectiveBlock implements FunctionParametersContainerInterface
{
    use CommaSeparatorWithNewLineTrait;
    use PassthroughItemCallbackTrait;
}
