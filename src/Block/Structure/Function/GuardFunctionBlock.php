<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\Statement\VariableReferenceBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AwaitInsideAsync;
use SunnyFlail\PhpTsBuilder\Constraint\ReturnsValueIfSpecified;
use SunnyFlail\PhpTsBuilder\Constraint\YieldInsideGenerator;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;

#[AwaitInsideAsync()]
#[YieldInsideGenerator()]
#[ReturnsValueIfSpecified()]
final readonly class GuardFunctionBlock extends AbstractFunctionBlock
{
    public function __construct(
        public string $name,
        ParameterBlock $parameter,
        VariableTypeBlockInterface $isAType,
        FunctionBodyBlock $body,
        ?MultilineCommentBlock $comment = null
    ) {
        $referencedVariable = new VariableReferenceBlock(
            iterator_to_array($parameter->getNames())[0]
        );

        parent::__construct(
            parameters: new ParametersBlock($parameter),
            returnType: new class($referencedVariable, $isAType) implements VariableTypeBlockInterface {
                public function __construct(
                    public VariableReferenceBlock $referencedVariable,
                    public VariableTypeBlockInterface $type
                ) {}

                public function toTypescript(TypescriptCodeSettings $settings): string
                {
                    return sprintf(
                        '%s is %s',
                        $this->referencedVariable->toTypescript($settings),
                        $this->type->toTypescript($settings)
                    );
                }
            },
            body: $body,
            comment: $comment
        );
    }

    protected function buildFunctionName(): string
    {
        return sprintf('function %s', $this->name);
    }
}
