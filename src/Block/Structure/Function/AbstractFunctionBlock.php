<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\AddPromiseToReturnType;
use SunnyFlail\PhpTsBuilder\Trait\IsMultilineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

abstract readonly class AbstractFunctionBlock implements FunctionBlockInterface
{
    use IsMultilineTrait;
    use SpacingTrait;
    use AddPromiseToReturnType;

    public function __construct(
        public ?FunctionParametersContainerInterface $parameters = null,
        public ?VariableTypeBlockInterface $returnType = null,
        public ?FunctionBodyBlock $body = null,
        public bool $async = false,
        public bool $generator = false,
        public ?MultilineCommentBlock $comment = null,
        public bool $addBody = true
    ) {
        ValidatorFacade::validate($this);
    }

    public function getReturnType(): ?VariableTypeBlockInterface
    {
        return $this->returnType;
    }

    public function getParameters(): array
    {
        return $this->parameters?->getItems() ?? [];
    }

    public function getBodies(): iterable
    {
        if ($this->body) {
            yield $this->body;
        }
    }

    public function getBodiesCount(): int
    {
        return iterator_count($this->getBodies());
    }

    public function isAsync(): bool
    {
        return $this->async;
    }

    public function isGenerator(): bool
    {
        return $this->generator;
    }

    final public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $lines = [];

        if ($this->comment) {
            $lines[] = $this->addSpacing($this->comment, $settings);
        }

        $lines[] = $this->buildFunctionDeclaration($settings);

        if ($this->addBody) {
            if ($this->body) {
                $lines[] = $this->addSpacing($this->body, $settings);
            }

            $lines[] = '}';
        }

        return implode($settings->newLineChar, $lines);
    }

    final protected function buildFunctionDeclaration(TypescriptCodeSettings $settings): string
    {
        $returnType = !$this->returnType ? false : $this->returnType;

        if ($returnType) {
            $returnType = $this->addPromiseToReturnType($this->async, $returnType);
        }

        $hasParameters = $this->parameters && 0 !== $this->parameters->getItemCount();
        $parameters = !$hasParameters ? '' : $this->addSpacing($this->parameters, $settings);

        $declaration = sprintf(
            '%1$s(%2$s%3$s%2$s)%4$s%5$s',
            $this->buildFunctionName(),
            '%1$s',
            '%2$s',
            !$returnType ? '' : sprintf(': %s', $returnType->toTypescript($settings)),
            $this->addBody ? ' {' : ''
        );
        $addNewlines = $hasParameters && $this->isMultiline(
            $settings,
            $declaration,
            '%1$s%2$s%1$s',
            $parameters
        );

        return sprintf(
            $declaration,
            $addNewlines ? $settings->newLineChar : '',
            $hasParameters ? ($addNewlines ? $parameters : trim($parameters)) : '',
        );
    }

    abstract protected function buildFunctionName(): string;
}
