<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\InvokableBlock;
use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AwaitInsideAsync;
use SunnyFlail\PhpTsBuilder\Constraint\CallbackConfiguredConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\ReturnsValueIfSpecified;
use SunnyFlail\PhpTsBuilder\Constraint\YieldInsideGenerator;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\AddPromiseToReturnType;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;
use SunnyFlail\PhpTsBuilder\Violation\DeltaWithoutBodyAndReturnTypes;

#[CallbackConfiguredConstraint(
    function (DeltaFunctionBlock $block): iterable {
        if (!$block->getBodiesCount() && !$block->getReturnType()) {
            yield new DeltaWithoutBodyAndReturnTypes();
        }
    }
)]
#[AwaitInsideAsync()]
#[YieldInsideGenerator()]
#[ReturnsValueIfSpecified()]
final readonly class DeltaFunctionBlock implements InvokableBlock, MultilineBlockInterface, FunctionBlockInterface
{
    use SpacingTrait;
    use AddPromiseToReturnType;

    public function __construct(
        public DeltaParametersBlock $parameters,
        public ?DeltaBodyBlock $body = null,
        public ?VariableTypeBlockInterface $returnType = null,
        public bool $async = false
    ) {
        ValidatorFacade::validate($this);
    }

    public function getReturnType(): ?VariableTypeBlockInterface
    {
        return $this->returnType;
    }

    public function getBodies(): iterable
    {
        if ($this->body) {
            yield $this->body;
        }
    }

    public function getBodiesCount(): int
    {
        return $this->body ? 1 : 0;
    }

    public function getParameters(): array
    {
        return $this->parameters->getItems();
    }

    public function isAsync(): bool
    {
        return $this->async;
    }

    public function isGenerator(): bool
    {
        return false;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $multipleParameters = !in_array($this->parameters->getItemCount(), [0, 1]);
        $parameters = sprintf(
            '%3$s(%1$s%2$s%1$s)',
            !$multipleParameters ? '' : $settings->newLineChar,
            !$multipleParameters ?
                $this->parameters->toTypescript($settings) :
                $this->addSpacing($this->parameters, $settings),
            !$this->async ? '' : 'async '
        );

        if ($this->returnType) {
            $parameters .= sprintf(
                ': %s',
                $this->addPromiseToReturnType(
                    $this->async,
                    $this->returnType
                )->toTypescript($settings)
            );
        }

        if ($this->body && 1 === $this->body->getItemCount()) {
            return sprintf(
                '%s => %s',
                $parameters,
                $this->body->toTypescript($settings)
            );
        }

        return sprintf(
            '%1$s => {%2$s%3$s%2$s}',
            $parameters,
            !$this->body ? '' : $settings->newLineChar,
            !$this->body ? '' : $this->body->toTypescript($settings)
        );
    }
}
