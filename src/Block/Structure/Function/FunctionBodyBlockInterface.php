<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\BodyBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyStatementBlockInterface;

/**
 * @template-extends BodyBlockInterface<BodyStatementBlockInterface>
 */
interface FunctionBodyBlockInterface extends BodyBlockInterface {}
