<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Function;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;

interface FunctionParameterBlockInterface extends UniqueNamedBlockInterface {}
