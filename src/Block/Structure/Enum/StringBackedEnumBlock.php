<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Enum;

use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Block\StructureBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\StructurePropertiesContainerInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueStructurePropertyNames;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

/**
 * @template-extends CollectiveBlock<StringBackedEnumCaseBlock>
 */
#[UniqueStructurePropertyNames()]
final readonly class StringBackedEnumBlock extends CollectiveBlock implements StructureBlockInterface, MultilineBlockInterface, StructurePropertiesContainerInterface
{
    use CommaSeparatorWithNewLineTrait;
    use SpacingItemCallbackTrait;

    public function __construct(
        public string $name,
        StringBackedEnumCaseBlock ...$cases
    ) {
        parent::__construct(...$cases);

        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }
}
