<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Enum;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\StructurePropertyBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ValueBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;

final readonly class StringBackedEnumCaseBlock implements BlockInterface, StructurePropertyBlockInterface
{
    public function __construct(
        public string $name,
        public ValueBlockInterface $value
    ) {
        ValidatorFacade::validate($this);
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        return sprintf(
            '%s = %s',
            $this->name,
            $this->value->toTypescript($settings)
        );
    }
}
