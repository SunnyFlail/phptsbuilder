<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Reusable\CollectiveBlock;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueStructurePropertyNames;
use SunnyFlail\PhpTsBuilder\Trait\PassthroughItemCallbackTrait;
use SunnyFlail\PhpTsBuilder\Trait\StructurePropertySeparatorTrait;

/**
 * @template-extends CollectiveBlock<ClassPropertyBlockInterface>
 */
#[UniqueStructurePropertyNames()]
abstract readonly class AbstractClassPropertiesBlock extends CollectiveBlock
{
    use PassthroughItemCallbackTrait;
    use StructurePropertySeparatorTrait;

    public function __construct(ClassPropertyBlockInterface ...$properties)
    {
        parent::__construct(...$properties);
    }
}
