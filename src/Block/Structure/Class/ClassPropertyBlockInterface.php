<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\StructurePropertyBlockInterface;

interface ClassPropertyBlockInterface extends StructurePropertyBlockInterface
{
    public function isStatic(): bool;
}
