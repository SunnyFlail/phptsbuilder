<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\DefaultExportableBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\MultilineBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\StructureBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueCollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\SpacingTrait;

/**
 * @template-implements UniqueCollectiveBlockInterface<ClassPropertyBlockInterface>
 */
final readonly class ClassBlock implements StructureBlockInterface, UniqueCollectiveBlockInterface, DefaultExportableBlockInterface, MultilineBlockInterface
{
    use SpacingTrait;

    public function __construct(
        public string|ClassNameBlock $name,
        public ?ConstructorBlock $constructor,
        public ?ClassPropertiesBlock $properties,
        public ?ClassMethodsBlock $methods,
        public ?ClassReferenceBlock $extendedClass = null,
        public ?ImplementedInterfacesBlock $implementedInterfaces = null,
        public ?MultilineCommentBlock $comment = null
    ) {}

    public function getItemCount(): int
    {
        return count($this->getItems());
    }

    public function getNames(): iterable
    {
        if ($this->name instanceof ClassNameBlock) {
            return $this->name->getNames();
        }

        yield $this->name;
    }

    public function getItems(): array
    {
        $items = [];

        if ($this->constructor) {
            foreach ($this->constructor->getParameters() as $property) {
                if ($property instanceof ClassPropertyBlockInterface) {
                    $items[] = $property;
                }
            }
        }

        if ($this->properties) {
            $items += $this->properties->getItems();
        }

        if ($this->methods) {
            $items += $this->methods->getItems();
        }

        return $items;
    }

    public function toTypescript(TypescriptCodeSettings $settings): string
    {
        $lines = [];

        if ($this->comment) {
            $lines[] = $this->addSpacing($this->comment, $settings);
        }

        $lines[] = sprintf(
            'readonly class %s%s%s',
            is_string($this->name) ?
                $this->name :
                $this->name->toTypescript($settings),
            !$this->extendedClass ?
                '' :
                sprintf(' extends %s', $this->extendedClass->toTypescript($settings)),
            !$this->implementedInterfaces ?
                '' :
                sprintf(' implements %s', $this->implementedInterfaces->toTypescript($settings))
        );

        $lines[] = '{';

        if ($this->properties) {
            $lines[] = $this->addSpacing($this->properties, $settings);
        }

        if ($this->constructor) {
            if ($this->properties) {
                $lines[] = '';
            }

            $lines[] = $this->addSpacing($this->constructor, $settings);
        }

        if ($this->methods) {
            if ($this->constructor || (null === $this->constructor && $this->properties)) {
                $lines[] = '';
            }

            $lines[] = $this->addSpacing(
                $this->methods,
                $settings
            );
        }

        $lines[] = '}';

        return implode($settings->newLineChar, $lines);
    }
}
