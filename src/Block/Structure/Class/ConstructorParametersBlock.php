<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Structure\Function\AbstractParametersBlock;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueFunctionParameterNames;

/**
 * @template-extends AbstractParametersBlock<ConstructorParameterBlockInterface>
 */
#[UniqueFunctionParameterNames()]
final readonly class ConstructorParametersBlock extends AbstractParametersBlock
{
    public function __construct(ConstructorParameterBlockInterface ...$parameters)
    {
        parent::__construct(...$parameters);
    }
}
