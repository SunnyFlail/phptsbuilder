<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Enum\AccessModifier;
use SunnyFlail\PhpTsBuilder\Block\Reusable\ObjectPropertylikeBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;

final readonly class PropertyBlock extends ObjectPropertylikeBlock implements ClassPropertyBlockInterface, ConstructorParameterBlockInterface
{
    public function __construct(
        string $name,
        VariableTypeBlockInterface $type,
        AccessModifier $accessModifier = AccessModifier::PUBLIC,
        bool $static = false,
        bool $readonly = false,
    ) {
        parent::__construct($name, $type, $accessModifier, $static, $readonly);
    }

    public function isStatic(): bool
    {
        return $this->static;
    }

    public function getAccessModifier(): AccessModifier
    {
        return $this->accessModifier ?? AccessModifier::PUBLIC;
    }
}
