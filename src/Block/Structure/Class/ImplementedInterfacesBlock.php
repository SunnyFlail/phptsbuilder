<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Reusable\UniqueCollectiveBlock;
use SunnyFlail\PhpTsBuilder\Constraint\AtLeastOneItem;
use SunnyFlail\PhpTsBuilder\Constraint\NonrepeatableStructureNameReferences;
use SunnyFlail\PhpTsBuilder\Trait\CommaSeparatorWithNewLineTrait;
use SunnyFlail\PhpTsBuilder\Trait\SpacingItemCallbackTrait;

/**
 * @template-extends UniqueCollectiveBlock<InterfaceReferenceBlock>
 */
#[AtLeastOneItem()]
#[NonrepeatableStructureNameReferences()]
final readonly class ImplementedInterfacesBlock extends UniqueCollectiveBlock
{
    use CommaSeparatorWithNewLineTrait;
    use SpacingItemCallbackTrait;

    public function __construct(InterfaceReferenceBlock ...$interfaces)
    {
        parent::__construct(...$interfaces);
    }
}
