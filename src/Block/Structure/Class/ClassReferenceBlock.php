<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractTemplatedTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\StructureReferenceBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueTemplateTypeNames;

#[UniqueTemplateTypeNames()]
final readonly class ClassReferenceBlock extends AbstractTemplatedTypeBlock implements StructureReferenceBlockInterface {}
