<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Enum\AccessModifier;
use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\AbstractFunctionBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\ParametersBlock;
use SunnyFlail\PhpTsBuilder\Block\VariableTypeBlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\AwaitInsideAsync;
use SunnyFlail\PhpTsBuilder\Constraint\ReturnsValueIfSpecified;
use SunnyFlail\PhpTsBuilder\Constraint\YieldInsideGenerator;

#[AwaitInsideAsync()]
#[YieldInsideGenerator()]
#[ReturnsValueIfSpecified()]
final readonly class MethodBlock extends AbstractFunctionBlock implements ClassPropertyBlockInterface
{
    public function __construct(
        public string $name,
        ?ParametersBlock $parameters,
        ?VariableTypeBlockInterface $returnType,
        ?FunctionBodyBlock $body,
        public ?AccessModifier $accessModifier = null,
        public bool $static = false,
        bool $async = false,
        bool $generator = false,
        ?MultilineCommentBlock $comment = null
    ) {
        parent::__construct(
            $parameters,
            $returnType,
            $body,
            $async,
            $generator,
            $comment
        );
    }

    public function isStatic(): bool
    {
        return $this->static;
    }

    public function getAccessModifier(): AccessModifier
    {
        return $this->accessModifier ?? AccessModifier::PUBLIC;
    }

    public function getNames(): iterable
    {
        yield $this->name;
    }

    protected function buildFunctionName(): string
    {
        return sprintf(
            '%s%s%s%s',
            !$this->accessModifier ? '' : sprintf('%s ', $this->accessModifier->value),
            !$this->async ? '' : 'async ',
            !$this->generator ? '' : '*',
            $this->name,
        );
    }
}
