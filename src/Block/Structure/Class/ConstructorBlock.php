<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Enum\AccessModifier;
use SunnyFlail\PhpTsBuilder\Block\MultilineCommentBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\AbstractFunctionBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBodyBlock;
use SunnyFlail\PhpTsBuilder\Constraint\CannotReturnValue;

#[CannotReturnValue()]
final readonly class ConstructorBlock extends AbstractFunctionBlock
{
    public function __construct(
        ConstructorParametersBlock $parameters,
        ?FunctionBodyBlock $body = null,
        public ?AccessModifier $accessModifier = null,
        ?MultilineCommentBlock $comment = null
    ) {
        parent::__construct(
            parameters: $parameters,
            returnType: null,
            body: $body,
            comment: $comment
        );
    }

    protected function buildFunctionName(): string
    {
        return sprintf(
            '%sconstructor',
            !$this->accessModifier ? '' : sprintf('%s ', $this->accessModifier->value)
        );
    }
}
