<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

final readonly class ClassPropertiesBlock extends AbstractClassPropertiesBlock
{
    public function __construct(PropertyBlock ...$properties)
    {
        parent::__construct(...$properties);
    }
}
