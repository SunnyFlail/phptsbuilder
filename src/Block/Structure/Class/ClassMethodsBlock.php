<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

final readonly class ClassMethodsBlock extends AbstractClassPropertiesBlock
{
    public function __construct(MethodBlock ...$methods)
    {
        parent::__construct(...$methods);
    }
}
