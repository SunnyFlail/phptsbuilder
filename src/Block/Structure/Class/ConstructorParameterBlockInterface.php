<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;

interface ConstructorParameterBlockInterface extends UniqueNamedBlockInterface {}
