<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Block\Structure\Class;

use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractTemplatedTypeBlock;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueTemplateTypeNames;

#[UniqueTemplateTypeNames()]
final readonly class ClassNameBlock extends AbstractTemplatedTypeBlock {}
