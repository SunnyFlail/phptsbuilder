<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Settings;

use SunnyFlail\PhpTsBuilder\Block\Enum\StructurePropertyDifferentiator;

final class MockSettings
{
    private static ?TypescriptCodeSettings $settings = null;

    private function __construct() {}

    public static function getMockSettings(): TypescriptCodeSettings
    {
        if (!self::$settings) {
            self::$settings = new TypescriptCodeSettings(
                120,
                '',
                '',
                StructurePropertyDifferentiator::SEMICOLON
            );
        }

        return self::$settings;
    }
}
