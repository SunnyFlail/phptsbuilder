<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Settings;

use SunnyFlail\PhpTsBuilder\Block\Enum\StructurePropertyDifferentiator;

readonly class TypescriptCodeSettings
{
    public function __construct(
        public int $lineLengthLimit,
        public string $lineStartWhitespace,
        public string $newLineChar,
        public StructurePropertyDifferentiator $structurePropertyDifferentiator
    ) {}
}
