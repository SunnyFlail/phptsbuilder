<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ReturnBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\YieldBlock;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;
use SunnyFlail\PhpTsBuilder\Violation\ReturnUsedOutsideOfFunction;
use SunnyFlail\PhpTsBuilder\Violation\YieldUsedOutsideOfGenerator;

/**
 * @template-implements BlockBodiesConstraint<BodyContainingBlockInterface,BlockInterface,ConstraintViolation>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class CannotReturnValue implements BlockBodiesConstraint
{
    use IsATrait;

    private array $violations = [];

    public function startValidation(BlockInterface $block): void
    {
        $this->guard($block);

        $this->violations = [];
    }

    public function validateItem(BlockInterface $validatedBlock, BlockInterface $item, int $index): void
    {
        if ($this->isA($item, ReturnBlock::class)) {
            $this->violations[] = new ReturnUsedOutsideOfFunction();
        }

        if ($this->isA($item, YieldBlock::class)) {
            $this->violations[] = new YieldUsedOutsideOfGenerator();
        }
    }

    public function getViolations(BlockInterface $block): array
    {
        return $this->violations;
    }

    /**
     * @psalm-assert BodyContainingBlockInterface $block
     */
    private function guard(BlockInterface $block): void
    {
        if (!$this->isA($block, BodyContainingBlockInterface::class)) {
            throw new ForbiddenConstraintTargetException($this, $block);
        }
    }
}
