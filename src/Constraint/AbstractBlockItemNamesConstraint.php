<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-covariant TBlock of CollectiveBlockInterface
 * @template-covariant TItem of UniqueNamedBlockInterface
 * @template-covariant TViolation of ConstraintViolation
 *
 * @template-implements BlockItemsConstraint<TBlock,TItem,TViolation>
 */
abstract class AbstractBlockItemNamesConstraint implements BlockItemsConstraint
{
    use IsATrait;

    /**
     * @var array<class-string<CollectiveBlockInterface>>
     */
    protected readonly array $allowedTargets;

    /**
     * @var ConstraintViolation[]
     */
    protected array $violations = [];

    /**
     * @var array<string,bool>
     */
    protected array $names = [];

    /**
     * @param class-string<CollectiveBlockInterface> $allowedTargets
     */
    public function __construct(string ...$allowedTargets)
    {
        $this->allowedTargets = $allowedTargets;
    }

    public function startValidation(BlockInterface $block): void
    {
        $this->guard($block);

        $this->violations = [];
        $this->names = [];
    }

    public function validateItem(
        BlockInterface $validatedBlock,
        BlockInterface $item,
        int $index
    ): void {
        if (!$this->isSearchItem($item)) {
            return;
        }

        /** @var UniqueNamedBlockInterface $item */
        foreach ($item->getNames() as $name) {
            if (isset($this->names[$name])) {
                $this->violations[] = $this->createViolation(
                    $validatedBlock,
                    $index,
                    $item,
                    $name
                );
            }

            $this->names[$name] = true;
        }
    }

    public function getViolations(BlockInterface $block): array
    {
        return $this->violations;
    }

    /**
     * @psalm-assert TBlock $block
     */
    protected function guard(BlockInterface $block): void
    {
        if (!$this->isA($block, ...$this->allowedTargets)) {
            throw new ForbiddenConstraintTargetException($this, $block);
        }
    }

    /**
     * @psalm-assert-if-true TItem $item
     */
    abstract protected function isSearchItem(BlockInterface $item): bool;

    /**
     * @param TBlock $block
     * @param TItem  $cause
     *
     * @return TViolation
     */
    abstract protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): ConstraintViolation;
}
