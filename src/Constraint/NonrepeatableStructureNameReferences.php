<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Structure\Class\ImplementedInterfacesBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Interface\ExtendedInterfacesBlock;
use SunnyFlail\PhpTsBuilder\Block\StructureReferenceBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedStructureNameReference;

/**
 * @template-extends AbstractBlockItemNamesConstraint<ImplementedInterfacesBlock,StructureReferenceBlockInterface,RepeatedStructureNameReference>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class NonrepeatableStructureNameReferences extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(
            ImplementedInterfacesBlock::class,
            ExtendedInterfacesBlock::class
        );
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, StructureReferenceBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedStructureNameReference {
        return new RepeatedStructureNameReference($name, $block, $cause);
    }
}
