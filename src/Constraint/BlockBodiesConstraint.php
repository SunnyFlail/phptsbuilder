<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-covariant TBlock of BodyContainingBlockInterface
 * @template-covariant TItem of BlockInterface
 * @template-covariant TViolation of ConstraintViolation
 *
 * @template-extends ItemConstraint<TBlock,TItem,TViolation>
 */
interface BlockBodiesConstraint extends ItemConstraint {}
