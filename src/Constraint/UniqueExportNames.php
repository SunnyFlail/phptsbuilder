<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ExportBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\FileBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedExportName;

/**
 * @template-extends AbstractBlockItemNamesConstraint<FileBlockInterface,ExportBlockInterface,RepeatedExportName>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueExportNames extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(FileBlockInterface::class);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, ExportBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedExportName {
        return new RepeatedExportName($name, $cause);
    }
}
