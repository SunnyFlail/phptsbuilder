<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-covariant TBlock of BlockInterface
 * @template-covariant TItem of BlockInterface
 * @template-covariant TViolation of ConstraintViolation
 */
interface ItemConstraint extends Constraint
{
    /**
     * @psalm-assert TBlock $block
     *
     * @throws ForbiddenConstraintTargetException if `$block` type is not `TBlock`
     */
    public function startValidation(BlockInterface $block): void;

    /**
     * @param TBlock $validatedBlock
     */
    public function validateItem(
        BlockInterface $validatedBlock,
        BlockInterface $item,
        int $index
    ): void;

    /**
     * @param TBlock $block
     *
     * @return iterable<TViolation>
     */
    public function getViolations(BlockInterface $block): iterable;
}
