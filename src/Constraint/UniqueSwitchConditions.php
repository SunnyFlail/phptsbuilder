<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\Switch\CaseBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\Switch\SwitchBlock;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedSameConditionCases;

/**
 * @template-extends AbstractBlockItemNamesConstraint<SwitchBlock,CaseBlockInterface,RepeatedSameConditionCases>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueSwitchConditions extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(SwitchBlock::class);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, CaseBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedSameConditionCases {
        return new RepeatedSameConditionCases($name, $cause);
    }
}
