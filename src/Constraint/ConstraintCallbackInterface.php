<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

interface ConstraintCallbackInterface
{
    /**
     * @return iterable<ConstraintViolation>
     */
    public function __invoke(
        BlockInterface $block,
        CallbackConfiguredConstraint $constraint,
    ): iterable;
}
