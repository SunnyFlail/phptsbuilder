<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Reusable\AbstractTemplatedTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\Type\TemplateExtendsTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedTemplateTypeName;

/**
 * @template-extends AbstractBlockItemNamesConstraint<AbstractTemplatedTypeBlock,TemplateExtendsTypeBlock,RepeatedTemplateTypeName>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueTemplateTypeNames extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(AbstractTemplatedTypeBlock::class);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $item instanceof TemplateExtendsTypeBlock;
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedTemplateTypeName {
        return new RepeatedTemplateTypeName($name, $cause);
    }
}
