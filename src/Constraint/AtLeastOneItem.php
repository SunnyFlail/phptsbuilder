<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Violation\InsufficientItems;

/**
 * @template-implements BlockConstraint<CollectiveBlockInterface,InsufficientItems>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final readonly class AtLeastOneItem implements BlockConstraint
{
    use IsATrait;

    /**
     * @param CollectiveBlockInterface $block
     *
     * @return iterable<InsufficientItems>
     */
    public function validate(BlockInterface $block): iterable
    {
        $this->guard($block);

        if (0 === $block->getItemCount()) {
            yield new InsufficientItems($block, $this);
        }
    }

    private function guard(BlockInterface $block): void
    {
        if (!$this->isA($block, CollectiveBlockInterface::class)) {
            throw new ForbiddenConstraintTargetException($this, $block);
        }
    }
}
