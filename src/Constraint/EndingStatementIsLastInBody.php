<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\EndingStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Violation\EndingStatementIsNotLastBodyItem;

/**
 * @template-implements BlockItemsConstraint<CollectiveBlockInterface,BlockInterface,EndingStatementIsNotLastBodyItem>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class EndingStatementIsLastInBody implements BlockItemsConstraint
{
    use IsATrait;

    private int $lastItemIndex = 0;

    /**
     * @var EndingStatementIsNotLastBodyItem[]
     */
    private array $violations = [];

    /**
     * @param CollectiveBlockInterface $block
     */
    public function startValidation(BlockInterface $block): void
    {
        $this->guard($block);

        $this->lastItemIndex = $block->getItemCount() - 1;
        $this->violations = [];
    }

    public function validateItem(
        BlockInterface $validatedBlock,
        BlockInterface $item,
        int $index
    ): void {
        if (
            $item instanceof EndingStatementBlockInterface
            && $index !== $this->lastItemIndex
        ) {
            $this->violations[] = new EndingStatementIsNotLastBodyItem();
        }
    }

    public function getViolations(BlockInterface $block): array
    {
        return $this->violations;
    }

    /**
     * @psalm-assert CollectiveBlockInterface $block
     */
    private function guard(BlockInterface $block): void
    {
        if (!$this->isA($block, CollectiveBlockInterface::class)) {
            throw new ForbiddenConstraintTargetException($this, $block);
        }
    }
}
