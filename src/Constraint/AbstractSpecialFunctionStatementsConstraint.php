<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-covariant TBlock of BodyContainingBlockInterface
 * @template-covariant TItem of BlockInterface
 * @template-covariant TViolation of ConstraintViolation
 *
 * @template-implements BlockBodiesConstraint<TBlock,TItem,TViolation>
 */
abstract class AbstractSpecialFunctionStatementsConstraint implements BlockBodiesConstraint
{
    use IsATrait;

    /**
     * @var TViolation[]
     */
    protected array $violations = [];

    protected bool $statementAllowed = false;

    /**
     * @var array<class-string<BlockInterface>>
     */
    protected readonly array $filteredStatements;

    /**
     * @param class-string<BodyContainingBlockInterface> $allowedTarget
     * @param class-string<BlockInterface>               $filteredStatements
     */
    public function __construct(
        protected string $allowedTarget,
        string ...$filteredStatements
    ) {
        $this->filteredStatements = $filteredStatements;
    }

    public function startValidation(BlockInterface $block): void
    {
        $this->guard($block);

        $this->statementAllowed = $this->isStatementAllowed($block);
    }

    public function validateItem(BlockInterface $validatedBlock, BlockInterface $item, int $index): void
    {
        if (
            !$this->statementAllowed
            && $this->isA($item, ...$this->filteredStatements)
        ) {
            $this->violations[] = $this->createViolation($validatedBlock, $item);
        }

    }

    public function getViolations(BlockInterface $block): array
    {
        return $this->violations;
    }

    /**
     * @psalm-assert TBlock $block
     */
    protected function guard(BlockInterface $block): void
    {
        if (!$this->isA($block, $this->allowedTarget)) {
            throw new ForbiddenConstraintTargetException($this, $block);
        }
    }

    /**
     * @param TBlock $block
     */
    abstract protected function createViolation(
        BodyContainingBlockInterface $block,
        BlockInterface $cause
    ): ConstraintViolation;

    /**
     * @param TBlock $block
     */
    abstract protected function isStatementAllowed(
        BodyContainingBlockInterface $block
    ): bool;
}
