<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\If\ElseBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\If\ElseBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\If\IfBlock;
use SunnyFlail\PhpTsBuilder\Violation\MultipleElseWithoutCondition;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedElseWithoutCondition;

/**
 * @template-extends RepeatedBlockType<IfBlock,ElseBlockInterface,MultipleElseWithoutCondition>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class AtMostOneElseWithoutCondition extends RepeatedBlockType
{
    public function __construct()
    {
        parent::__construct(IfBlock::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        BlockInterface $cause
    ): RepeatedElseWithoutCondition {
        return new RepeatedElseWithoutCondition();
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, ElseBlock::class);
    }
}
