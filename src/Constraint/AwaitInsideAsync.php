<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Value\AwaitBlock;
use SunnyFlail\PhpTsBuilder\Violation\AwaitUsedOutsideAsyncFunction;

/**
 * @template-extends AbstractSpecialFunctionStatementsConstraint<FunctionBlockInterface,BlockInterface,AwaitUsedOutsideAsyncFunction>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class AwaitInsideAsync extends AbstractSpecialFunctionStatementsConstraint
{
    public function __construct()
    {
        parent::__construct(FunctionBlockInterface::class, AwaitBlock::class);
    }

    protected function createViolation(
        BodyContainingBlockInterface $block,
        BlockInterface $cause
    ): AwaitUsedOutsideAsyncFunction {
        return new AwaitUsedOutsideAsyncFunction();
    }

    /**
     * @param FunctionBlockInterface $block
     */
    protected function isStatementAllowed(BodyContainingBlockInterface $block): bool
    {
        return $block->isAsync();
    }
}
