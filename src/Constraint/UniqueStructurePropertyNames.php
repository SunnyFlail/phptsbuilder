<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\StructurePropertiesContainerInterface;
use SunnyFlail\PhpTsBuilder\Block\StructurePropertyBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedPropertyName;

/**
 * @template-extends AbstractBlockItemNamesConstraint<StructurePropertiesContainerInterface,StructurePropertyBlockInterface,RepeatedPropertyName>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueStructurePropertyNames extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(StructurePropertiesContainerInterface::class);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, StructurePropertyBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedPropertyName {
        return new RepeatedPropertyName($name, $cause);
    }
}
