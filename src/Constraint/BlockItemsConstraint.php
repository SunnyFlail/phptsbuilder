<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-covariant TBlock of CollectiveBlockInterface
 * @template-covariant TItem of BlockInterface
 * @template-covariant TViolation of ConstraintViolation
 *
 * @template-extends ItemConstraint<TBlock,TItem,TViolation>
 */
interface BlockItemsConstraint extends ItemConstraint {}
