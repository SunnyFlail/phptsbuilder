<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyContainingBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\YieldBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\YieldUsedOutsideOfGenerator;

/**
 * @template-extends AbstractSpecialFunctionStatementsConstraint<FunctionBlockInterface,BlockInterface,YieldUsedOutsideOfGenerator>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class YieldInsideGenerator extends AbstractSpecialFunctionStatementsConstraint
{
    public function __construct()
    {
        parent::__construct(FunctionBlockInterface::class, YieldBlock::class);
    }

    protected function createViolation(
        BodyContainingBlockInterface $block,
        BlockInterface $cause
    ): YieldUsedOutsideOfGenerator {
        return new YieldUsedOutsideOfGenerator();
    }

    /**
     * @param FunctionBlockInterface $block
     */
    protected function isStatementAllowed(BodyContainingBlockInterface $block): bool
    {
        return $block->isGenerator();
    }
}
