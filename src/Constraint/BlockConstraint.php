<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-covariant TBlock of BlockInterface
 * @template-covariant TViolation of ConstraintViolation
 */
interface BlockConstraint extends Constraint
{
    /**
     * @param TBlock $block
     *
     * @return iterable<TViolation>
     *
     * @throws ForbiddenConstraintTargetException if `$block` is not of type `TBlock`
     */
    public function validate(BlockInterface $block): iterable;
}
