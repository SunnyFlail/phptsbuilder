<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionParameterBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionParametersContainerInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedParameterName;

/**
 * @template-extends AbstractBlockItemNamesConstraint<FunctionParametersContainerInterface,FunctionParameterBlockInterface,RepeatedParameterName>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueFunctionParameterNames extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(FunctionParametersContainerInterface::class);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, FunctionParameterBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedParameterName {
        return new RepeatedParameterName($name, $cause);
    }
}
