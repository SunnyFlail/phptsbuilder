<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\BodyBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ReturnBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\ThrowBlock;
use SunnyFlail\PhpTsBuilder\Block\EndingStatementBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedEndingStatement;

/**
 * @template-extends RepeatedBlockType<BodyBlockInterface,EndingStatementBlockInterface,RepeatedEndingStatement>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class SingleEndingStatement extends RepeatedBlockType
{
    public function __construct()
    {
        parent::__construct(BodyBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        BlockInterface $cause
    ): RepeatedEndingStatement {
        return new RepeatedEndingStatement($index, $block, $cause);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, ReturnBlock::class, ThrowBlock::class);
    }
}
