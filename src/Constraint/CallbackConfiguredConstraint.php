<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

/**
 * @template-implements BlockConstraint<BlockInterface,ConstraintViolation>
 */
#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::IS_REPEATABLE)]
final readonly class CallbackConfiguredConstraint implements BlockConstraint
{
    public function __construct(
        public ConstraintCallbackInterface|\Closure $callback,
    ) {}

    public function validate(BlockInterface $block): iterable
    {
        $callback = $this->callback;

        return $callback($block, $this);
    }
}
