<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ReturnBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Type\VoidVariableTypeBlock;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Trait\IsATrait;
use SunnyFlail\PhpTsBuilder\Violation\NoReturnStatementInBody;

/**
 * @template-implements BlockBodiesConstraint<FunctionBlockInterface,BlockInterface,NoReturnStatementInBody>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class ReturnsValueIfSpecified implements BlockBodiesConstraint
{
    use IsATrait;

    protected bool $mustReturn = false;
    protected bool $hasReturnStatement = false;

    /**
     * @param FunctionBlockInterface $block
     */
    public function startValidation(BlockInterface $block): void
    {
        $this->guard($block);

        $returnType = $block->getReturnType();

        $this->mustReturn = $returnType
            && !$this->isA($returnType, VoidVariableTypeBlock::class)
        ;
        $this->hasReturnStatement = false;
    }

    public function validateItem(
        BlockInterface $validatedBlock,
        BlockInterface $item,
        int $index
    ): void {
        if ($this->mustReturn) {
            if ($this->isA($item, ReturnBlock::class)) {
                $this->hasReturnStatement = true;
            }
        }
    }

    public function getViolations(BlockInterface $block): iterable
    {
        if ($this->mustReturn && !$this->hasReturnStatement) {
            yield new NoReturnStatementInBody($block);
        }
    }

    /**
     * @psalm-assert FunctionBlockInterface $block
     */
    protected function guard(BlockInterface $block): void
    {
        if (!$this->isA($block, FunctionBlockInterface::class)) {
            throw new ForbiddenConstraintTargetException($this, $block);
        }
    }
}
