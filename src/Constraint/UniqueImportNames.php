<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Constraint;

use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\CollectiveBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\FileBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\ImportBlockInterface;
use SunnyFlail\PhpTsBuilder\Block\UniqueNamedBlockInterface;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedImportName;

/**
 * @template-extends AbstractBlockItemNamesConstraint<FileBlockInterface,ImportBlockInterface,RepeatedImportName>
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
final class UniqueImportNames extends AbstractBlockItemNamesConstraint
{
    public function __construct()
    {
        parent::__construct(FileBlockInterface::class);
    }

    protected function isSearchItem(BlockInterface $item): bool
    {
        return $this->isA($item, ImportBlockInterface::class);
    }

    protected function createViolation(
        CollectiveBlockInterface $block,
        int $index,
        UniqueNamedBlockInterface $cause,
        string $name
    ): RepeatedImportName {
        return new RepeatedImportName($name, $cause);
    }
}
