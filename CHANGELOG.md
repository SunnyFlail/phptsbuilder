# 3.5.0
## Added:
- added `BracketBlock`

# 3.4.0
## Changed:
- changed `VariableDeclarationBlock::value` property type to `nullable`
- changed `AbstractDeconstructionBlock::source` property type to `nullable`

# 3.3.0
## Changed:
- made all classes implementing `BlockInterface` to be readonly, and changed most of their properties to be public  

# 3.2.0
## Added:
- added `NullValueBlock`

# 3.1.2
## Fixed:
- fixed `SwitchBlock`, `IfBlock`, `ElseIfBlock`, `MethodBlock`, `AbstractFunctionBlock`, `FunctionBlock`, `DeltaFunctionBlock` to not added new line chars if arguments are not multiline

# 3.1.1
## Fixed:
- changed `FunctionInvocationBlock` to not add new line chars if there are no arguments

# 3.1.0
## Added:
- added `ArrayDelimitedObjectPropertyName`

# 3.0.0
## Added:
- added `ConstraintRunnerInterface`
- added `ConstraintRunnerAggregate` for running all constraint validations
- Added `UniqueImportNames`, `UniqueExportNames`, `CannotReturnValue` constraints
- added `Constraint` as base interface for all constraints

## Changes:
- major constraint overhaul - added `ConstraintRunnerInterface` for ease of creating custom constraints based on other properties, extending `Constraint` interface
- iterating constraints now run in pseudo-pararell

# 2.5.0
## Added:
- added `GuardFunctionBlock` replacing functionality of `IsAVariableTypeBlock`
- added `VoidVariableTypeBlock`
- added `NegationOperationBlock`
- added `MultilineCommentBlock`
- added `ExtendedInterfacesBlock`
- added `InterfaceNameBlock` as possible argument type for `InterfaceBlock::$name`
- added `BodyContainingBlockInterface::getBodiesCount` method

## Changed:
- renamed `InOperator` to `InOperatorBlock` 
- added `MultilineCommentBlock|null $comment` property to `AbstractFunctionBlock`, `FunctionBlock`, `MethodBlock`, `ConstructorBlock`, `ClassBlock`, `InterfaceBlock`, `InterfaceMethodBlock`
- added `ExtendedInterfacesBlock|null $extendedInterfaces` property to `InterfaceBlock`

## Removed:
- removed `IsAVariableTypeBlock`

# 2.4.0
## Added:
- added `OperationType::AND` and `OperationType::OR`

# 2.3.0
## Added:
- added `IsAVariableTypeBlock` for ts type guard functions

# 2.2.0
## Added:
- added `TemplateExtendsTypeBlock` for creating generic types extending other type
- added `KeyOfVariableTypeBlock`
- added `UniqueTemplateTypeNames` constraint
- added `ClassNameBlock` as possible argument type for `ClassBlock::$name`

# 2.1.0
## Added:
- added `AwaitInsideAsync` constraint 
- added `YieldInsideGenerator` constraint 

## Fixed:
- added validation error when passing `ReturnBlock` or `YieldBlock` as a line in body for `ConstructorBlock` 

# 2.0.0
## Added:
- added attribute based block validation

## Changed:
- Moved `ImportBlock` to namespace `SunnyFlail\PhpTsBuilder\Block\Control\Export`
- Moved `FunctionBodyBlock` and `DeltaBodyBlock` to namespace `SunnyFlail\PhpTsBuilder\Block\Structure\Function`
- Renamed `ArgumentBlock` to `ParameterBlock`
- Renamed `ArgumentsBlock` to `ParametersBlock`
- Renamed `AbstractArgumentsBlock` to `AbstractParametersBlock`
- Renamed `DeltaArgumentsBlock` to `DeltaParametersBlock`
- Renamed `ConstructorArgumentsBlock` to `ConstructorParametersBlock`
- Renamed `ConstructorArgumentBlockInterface` to `ConstructorParameterBlockInterface`

## Fixed:
- Added missing comment block to `TsIgnoreBlock`

## Removed:
- removed all legacy validation exceptions
- removed `ValidateNonrepeatableItemsTrait`
- removed `ValidateUniqueNamedItemsTrait`
- removed `InterfaceBodyBlock`

# 1.4.0
## Added:
- added `IsEmptyLineTrait`
- added `LineEndSemicolonItemCallbackTrait`
- added `MultilineBlockInterface`
- added tests

## Changed:
- changed type of argument `AbstractFunctionBlock::body` to `FunctionBodyBlock`
- changed `FileBlock` to not add semicolon at end of Multiline body blocks

## Fixed:
- fixed `AbstractBodyBlock` to not add semicolon at end of Multiline body blocks

# 1.3.0
## Changed:
- renamed `AbstractTemplatedTypeBlock` to `TemplatedVariableTypeBlock`
- renamed `IntersectionTypeBlock` to `IntersectionVariableTypeBlock`
- changed namespace of `ClassReferenceBlock` and `InterfaceReferenceBlock` to `SunnyFlail\PhpTsBuilder\Block\Structure\Class`
- removed implementation of `VariableTypeBlockInterface` from `ClassReferenceBlock` and `InterfaceReferenceBlock`

## Fixed:
- renamed `ClassRefenceBlock` to `ClassReferenceBlock`
- fixed changelog formatting 

## Removed:
- removed `AbstractStructureReferenceBlock`

# 1.2.0
## Changed:
- added implementation of `UniqueNamedBlockInterface` to `TypeBlock`

# 1.1.1
## Fixed
- added missing interface implementation `VariableTypeBlockInterface` to `IntersectionTypeBlock`

# 1.1.0
## Added:
- added `ImportBlock`
- added `FileBodyStatementBlockInterface`

## Changed:
- changed `FileBlock::lines` argument type to `FileBodyStatementBlockInterface[]`

# 1.0.0
## Added:
- Created package