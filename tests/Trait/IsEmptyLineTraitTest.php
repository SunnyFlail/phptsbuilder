<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Tests\Trait;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SunnyFlail\PhpTsBuilder\Block\Enum\StructurePropertyDifferentiator;
use SunnyFlail\PhpTsBuilder\Settings\TypescriptCodeSettings;
use SunnyFlail\PhpTsBuilder\Trait\IsEmptyLineTrait;

final class IsEmptyLineTraitTest extends TestCase
{
    private static TypescriptCodeSettings $settings;

    public static function setUpBeforeClass(): void
    {
        static::$settings = new TypescriptCodeSettings(
            120,
            '    ',
            PHP_EOL,
            StructurePropertyDifferentiator::SEMICOLON
        );
    }

    public static function dataIsEmptyLine(): iterable
    {
        yield 'Empty line' => [
            'testedString' => '   ',
            'expected' => true,
        ];

        yield 'Semicolon' => [
            'testedString' => ';',
            'expected' => true,
        ];

        yield 'new line' => [
            'testedString' => PHP_EOL.';'.PHP_EOL,
            'expected' => true,
        ];

        yield 'Block start' => [
            'testedString' => PHP_EOL.'  {'.PHP_EOL,
            'expected' => true,
        ];
    }

    #[DataProvider('dataIsEmptyLine')]
    public function testIsEmptyLine(string $testedString, bool $expected): void
    {
        $SUT = new class(static::$settings) {
            use IsEmptyLineTrait;

            public function __construct(private TypescriptCodeSettings $settings) {}

            public function __invoke(string $line): bool
            {
                return $this->isEmptyLine($line, $this->settings);
            }
        };

        $this->assertEquals($expected, $SUT($testedString));
    }
}
