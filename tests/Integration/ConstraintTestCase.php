<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Tests\Integration;

use PHPUnit\Framework\TestCase;
use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;
use SunnyFlail\PhpTsBuilder\Exception\BlockValidationFailureException;
use SunnyFlail\PhpTsBuilder\Exception\ForbiddenConstraintTargetException;
use SunnyFlail\PhpTsBuilder\Validator\BlockBodiesConstraintRunner;
use SunnyFlail\PhpTsBuilder\Validator\BlockConstraintRunner;
use SunnyFlail\PhpTsBuilder\Validator\BlockDeclaration;
use SunnyFlail\PhpTsBuilder\Validator\BlockDeclarationTracerInterface;
use SunnyFlail\PhpTsBuilder\Validator\BlockItemsConstraintRunner;
use SunnyFlail\PhpTsBuilder\Validator\ConstraintProviderInterface;
use SunnyFlail\PhpTsBuilder\Validator\ConstraintRunnerAggregate;
use SunnyFlail\PhpTsBuilder\Validator\Validator;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorFacade;
use SunnyFlail\PhpTsBuilder\Validator\ValidatorInterface;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;

abstract class ConstraintTestCase extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        ValidatorFacade::disable();
    }

    final protected function expectForbiddenConstraintTargetException(
        BlockInterface $block,
        Constraint $constraint
    ): void {
        $validator = $this->createValidator($block, $constraint);

        $this->expectException(ForbiddenConstraintTargetException::class);

        $validator->validateBlock($block);
    }

    final protected function expectBlockDoesNotViolate(
        BlockInterface $block,
        Constraint $constraint
    ): void {
        $validator = $this->createValidator($block, $constraint);
        $this->expectNotToPerformAssertions();
        $validator->validateBlock($block);
    }

    /**
     * @param class-string<ConstraintViolation> $expectedViolations
     */
    final protected function expectBlockViolates(
        BlockInterface $block,
        Constraint $constraint,
        string ...$expectedViolations
    ): void {
        $validator = $this->createValidator($block, $constraint);
        $expectedViolationsCounts = $this->countViolations(...$expectedViolations);
        $actualViolationsCounts = [];
        $validationFailed = false;

        try {
            $validator->validateBlock($block);
        } catch (BlockValidationFailureException $e) {
            $validationFailed = true;
            $actualViolationsCounts = $this->countViolations(
                ...array_map(
                    fn (ConstraintViolation $violation) => $violation::class,
                    $e->violations
                )
            );
        }

        if (!$validationFailed) {
            $this->fail('No violations occurred');

            return;
        }

        $this->assertEqualsCanonicalizing(
            $expectedViolationsCounts,
            $actualViolationsCounts,
            ''
        );
    }

    private function countViolations(string ...$violationClasses): array
    {
        $violationCounts = [];

        foreach ($violationClasses as $violation) {
            if (!isset($violationCounts[$violation])) {
                $violationCounts[$violation] = 0;
            }

            ++$violationCounts[$violation];
        }

        return $violationCounts;
    }

    private function createValidator(
        BlockInterface $block,
        Constraint $constraint
    ): ValidatorInterface {
        return new Validator(
            $this->createConstraintProvider($block, $constraint),
            $this->createDeclarationTracer(),
            new ConstraintRunnerAggregate(
                new BlockConstraintRunner(),
                new BlockItemsConstraintRunner(),
                new BlockBodiesConstraintRunner(),
            )
        );
    }

    private function createDeclarationTracer(): BlockDeclarationTracerInterface
    {
        return new class() implements BlockDeclarationTracerInterface {
            public function traceBlockDeclaration(BlockInterface $block): BlockDeclaration
            {
                return new BlockDeclaration(
                    '',
                    0,
                    [],
                    []
                );
            }
        };
    }

    private function createConstraintProvider(
        BlockInterface $block,
        Constraint $constraint
    ): ConstraintProviderInterface {
        $map = [$block::class => [$constraint]];

        return new class($map) implements ConstraintProviderInterface {
            public function __construct(private array $map) {}

            public function getBlockConstraints(BlockInterface $block): iterable
            {
                return $this->map[$block::class] ?? [];
            }
        };
    }
}
