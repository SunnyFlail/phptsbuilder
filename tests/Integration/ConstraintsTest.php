<?php

declare(strict_types=1);

namespace SunnyFlail\PhpTsBuilder\Tests\Integration;

use PHPUnit\Framework\Attributes\DataProvider;
use SunnyFlail\PhpTsBuilder\Block\BlockInterface;
use SunnyFlail\PhpTsBuilder\Block\Control\ControlBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\If\ElseBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\If\IfBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\ReturnBlock;
use SunnyFlail\PhpTsBuilder\Block\Control\YieldBlock;
use SunnyFlail\PhpTsBuilder\Block\FileBlock;
use SunnyFlail\PhpTsBuilder\Block\Statement\VariableReferenceBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\FunctionBodyBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\ParameterBlock;
use SunnyFlail\PhpTsBuilder\Block\Structure\Function\ParametersBlock;
use SunnyFlail\PhpTsBuilder\Block\Type\SimpleVariableTypeBlock;
use SunnyFlail\PhpTsBuilder\Block\Value\AwaitBlock;
use SunnyFlail\PhpTsBuilder\Block\Value\BooleanValueBlock;
use SunnyFlail\PhpTsBuilder\Constraint\AbstractBlockItemNamesConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\AbstractSpecialFunctionStatementsConstraint;
use SunnyFlail\PhpTsBuilder\Constraint\AtMostOneElseWithoutCondition;
use SunnyFlail\PhpTsBuilder\Constraint\AwaitInsideAsync;
use SunnyFlail\PhpTsBuilder\Constraint\CannotReturnValue;
use SunnyFlail\PhpTsBuilder\Constraint\Constraint;
use SunnyFlail\PhpTsBuilder\Constraint\EndingStatementIsLastInBody;
use SunnyFlail\PhpTsBuilder\Constraint\RepeatedBlockType;
use SunnyFlail\PhpTsBuilder\Constraint\ReturnsValueIfSpecified;
use SunnyFlail\PhpTsBuilder\Constraint\UniqueFunctionParameterNames;
use SunnyFlail\PhpTsBuilder\Violation\AwaitUsedOutsideAsyncFunction;
use SunnyFlail\PhpTsBuilder\Violation\ConstraintViolation;
use SunnyFlail\PhpTsBuilder\Violation\EndingStatementIsNotLastBodyItem;
use SunnyFlail\PhpTsBuilder\Violation\NoReturnStatementInBody;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedElseWithoutCondition;
use SunnyFlail\PhpTsBuilder\Violation\RepeatedParameterName;
use SunnyFlail\PhpTsBuilder\Violation\ReturnUsedOutsideOfFunction;
use SunnyFlail\PhpTsBuilder\Violation\YieldUsedOutsideOfGenerator;

final class ConstraintsTest extends ConstraintTestCase
{
    public static function dataBlockViolatesConstraint(): iterable
    {
        yield RepeatedBlockType::class => [
            'block' => new IfBlock(
                new BooleanValueBlock(true),
                new ControlBodyBlock(),
                new ElseBlock(new ControlBodyBlock()),
                new ElseBlock(new ControlBodyBlock()),
            ),
            'constraint' => new AtMostOneElseWithoutCondition(),
            'expectedViolations' => [RepeatedElseWithoutCondition::class],
        ];

        yield EndingStatementIsLastInBody::class => [
            'block' => new ControlBodyBlock(
                new ReturnBlock(),
                new VariableReferenceBlock('test')
            ),
            'constraint' => new EndingStatementIsLastInBody(),
            'expectedViolations' => [EndingStatementIsNotLastBodyItem::class],
        ];

        yield AbstractBlockItemNamesConstraint::class => [
            'block' => new ParametersBlock(
                new ParameterBlock(
                    'name',
                    new SimpleVariableTypeBlock('string')
                ),
                new ParameterBlock(
                    'name',
                    new SimpleVariableTypeBlock('string')
                ),
            ),
            'constraint' => new UniqueFunctionParameterNames(),
            'expectedViolations' => [RepeatedParameterName::class],
        ];

        yield AbstractSpecialFunctionStatementsConstraint::class => [
            'block' => new FunctionBlock(
                'test',
                null,
                null,
                new FunctionBodyBlock(
                    new AwaitBlock(new BooleanValueBlock(true))
                )
            ),
            'constraint' => new AwaitInsideAsync(),
            'expectedViolations' => [AwaitUsedOutsideAsyncFunction::class],
        ];

        yield CannotReturnValue::class => [
            'block' => new FileBlock(
                new IfBlock(
                    new BooleanValueBlock(true),
                    new ControlBodyBlock(
                        new YieldBlock(new BooleanValueBlock(true)),
                        new ReturnBlock()
                    )
                )
            ),
            'constraint' => new CannotReturnValue(),
            'expectedViolations' => [
                ReturnUsedOutsideOfFunction::class,
                YieldUsedOutsideOfGenerator::class,
            ],
        ];

        yield ReturnsValueIfSpecified::class => [
            'block' => new FunctionBlock(
                '',
                null,
                new SimpleVariableTypeBlock('string'),
                new FunctionBodyBlock()
            ),
            'constraint' => new ReturnsValueIfSpecified(),
            'expectedViolations' => [NoReturnStatementInBody::class],
        ];
    }

    /**
     * @param ConstraintViolation[] $expectedViolations
     */
    #[DataProvider('dataBlockViolatesConstraint')]
    public function testBlockViolatesConstraint(
        BlockInterface $block,
        Constraint $constraint,
        array $expectedViolations
    ): void {
        $this->expectBlockViolates($block, $constraint, ...$expectedViolations);
    }
}
